(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ 441:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/group.md?vue&type=template&id=3db1df1c

var groupvue_type_template_id_3db1df1c_hoisted_1 = {
  class: "doc-main-content"
};
var groupvue_type_template_id_3db1df1c_hoisted_2 = {
  class: "doc-content"
};

var groupvue_type_template_id_3db1df1c_hoisted_3 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h1", null, "Group 组", -1);

var groupvue_type_template_id_3db1df1c_hoisted_4 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "组 Group 是鸿鸾中最具有特点的功能之一，它更多用于页面元素的布局构造，当然不仅如此。", -1);

var groupvue_type_template_id_3db1df1c_hoisted_5 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "ji-ben-shi-yong"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#ji-ben-shi-yong"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 基本使用")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_6 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("通常我们将元素与元素的组合用"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "<div>"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("包裹起来，此时不妨尝试将它们放到一个"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "group"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("组里")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_7 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "group"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("组件会自动为组成员增加 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, ".group-item"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 类。")])], -1);

var groupvue_type_template_id_3db1df1c_hoisted_8 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-group class=\"dom-area\" data-name=\"G1\">\n  <hl-input placeholder=\"请输入内容\" class=\"m-r-sm\"></hl-input>\n  <hl-button type=\"primary\">按钮第二</hl-button>\n</hl-group>\n")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_9 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "pai-lie-fang-shi"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#pai-lie-fang-shi"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 排列方式")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_10 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("为 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "<hl-group>"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 设置 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "dir=\"horizontal\""), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 或 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "dir=\"vertical\""), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 属性即可改版它们的排列方式。")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_11 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("当你未定义 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "<hl-group>"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 的 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "dir=\"\""), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 属性时，默认为 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "horizontal"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 横向排列。")])], -1);

var groupvue_type_template_id_3db1df1c_hoisted_12 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-row>\n  <hl-col span=\"col\">\n    <p>横向排列 horizontal</p>\n    <hl-group dir=\"horizontal\">\n      <hl-button type=\"primary\" class=\"m-r-xs\">Group Item</hl-button>\n      <hl-button type=\"primary\">Group Item</hl-button>\n    </hl-group>\n  </hl-col>\n  <hl-col span=\"col\">\n    <p>纵向排列 vertical</p>\n    <hl-group dir=\"vertical\">\n      <hl-button type=\"primary\" class=\"m-b-xs\">Group Item</hl-button>\n      <hl-button type=\"primary\">Group Item</hl-button>\n    </hl-group>\n  </hl-col>\n</hl-row>\n")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_13 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "rong-he"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#rong-he"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 融合")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_14 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "增加 merge 属性可使组内成员非常友好的融合。哪怕组内成员拥有各自的边距和圆角也能被正确的处理。", -1);

var groupvue_type_template_id_3db1df1c_hoisted_15 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-row>\n  <hl-col span=\"col\">\n    <p>横向融合</p>\n    <hl-group dir=\"horizontal\" merge>\n      <hl-button type=\"primary\" class=\"m-r-xs\">Group Item</hl-button>\n      <hl-button type=\"primary\">Group Item</hl-button>\n    </hl-group>\n    <br>\n    <hl-group dir=\"horizontal\" merge class=\"m-t-lg\">\n      <hl-button type=\"primary\" outline class=\"m-r-xs\">Group Item</hl-button>\n      <hl-button type=\"primary\" outline>Group Item</hl-button>\n    </hl-group>\n  </hl-col>\n  <hl-col span=\"col\">\n    <p>纵向融合</p>\n    <hl-group dir=\"vertical\" merge>\n      <hl-button type=\"primary\" class=\"m-b-xs\">Group Item</hl-button>\n      <hl-button type=\"primary\">Group Item</hl-button>\n    </hl-group>\n    <hl-group dir=\"vertical\" merge class=\"m-l-lg\">\n      <hl-button type=\"primary\" outline class=\"m-b-xs\">Group Item</hl-button>\n      <hl-button type=\"primary\" outline>Group Item</hl-button>\n    </hl-group>\n  </hl-col>\n</hl-row>\n")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_16 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "suo-jin"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#suo-jin"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 缩进")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_17 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("缩进标签 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "indent"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 用于缩减组成员之间的距离，你可以直观的看到融合的元素会自动减去相连接的边框。")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_18 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-row>\n    <hl-col span=\"col-md-12\">\n        <p>横向缩进融合</p>\n        <hl-group dir=\"horizontal\" class=\"m-t-sm\" merge indent>\n            <hl-button type=\"primary\" outline>Group Item</hl-button>\n            <hl-button type=\"primary\" outline>Group Item</hl-button>\n          </hl-group>\n    </hl-col>\n    <hl-col span=\"col-md-12\">\n        <p>纵向缩进融合</p>\n        <hl-group dir=\"vertical\" class=\"m-t-sm\" merge indent>\n            <hl-button type=\"primary\" outline>Group Item</hl-button>\n            <hl-button type=\"primary\" outline>Group Item</hl-button>\n          </hl-group>\n    </hl-col>\n\n    <hl-col span=\"col\" class=\"m-t-xl\">\n        <p>你也可以设置具体数值<code>indent=\"-2px\"</code>来设置缩进距离</p>\n        <hl-group dir=\"horizontal\" class=\"m-t-sm\" merge indent=\"-2px\">\n            <hl-button type=\"primary\" style=\"border-width: 2px\" outline>Group Item</hl-button>\n            <hl-button type=\"primary\" style=\"border-width: 2px\" outline>Group Item</hl-button>\n          </hl-group>\n    </hl-col>\n    <hl-col span=\"col\" class=\"m-t-xl\">\n        <p>正值设置<code>indent=\"1vw\"</code></p>\n        <hl-group dir=\"horizontal\" class=\"m-t-sm\" indent=\"1vw\">\n            <hl-button type=\"primary\" style=\"border-width: 2px\" outline>Group Item</hl-button>\n            <hl-button type=\"primary\" style=\"border-width: 2px\" outline>Group Item</hl-button>\n          </hl-group>\n    </hl-col>\n</hl-row>\n\n\n")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_19 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "zi-gua-ying"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#zi-gua-ying"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 自适应")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_20 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("为 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "group"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 增加 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "full"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 标签，可使组和组内成员都拉伸至父元素的宽度或高度。")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_21 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-row>\n  <hl-col span=\"col\">\n    <hl-group full>\n      <hl-button type=\"primary\" class=\"m-r-sm\" outline>Auto</hl-button>\n      <hl-button type=\"primary\" outline>Auto</hl-button>\n    </hl-group>\n  </hl-col>\n\n  <hl-col span=\"col\">\n    <hl-group merge indent full>\n      <hl-button type=\"primary\" class=\"m-r-sm\" outline>Auto</hl-button>\n      <hl-button type=\"primary\" outline>Auto</hl-button>\n    </hl-group>\n  </hl-col>\n\n</hl-row>\n<p>使用<code>vertical</code>样式将组纵向排列后，会自动变为高度自适应</p>\n<hl-row class=\"m-t-md\">\n  <hl-col span=\"col\">\n    <hl-group dir=\"vertical\" full style=\"height: 120px;\">\n      <hl-button type=\"primary\" class=\"m-b-sm\" outline>Auto</hl-button>\n      <hl-button type=\"primary\" outline>Auto</hl-button>\n    </hl-group>\n  </hl-col>\n\n  <hl-col span=\"col\">\n    <hl-group dir=\"vertical\" merge indent full style=\"height: 120px;\">\n      <hl-button type=\"primary\" class=\"m-b-sm\" outline>Auto</hl-button>\n      <hl-button type=\"primary\" outline>Auto</hl-button>\n    </hl-group>\n  </hl-col>\n\n</hl-row>\n")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_22 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "jing-tai-zu-cheng-yuan"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#jing-tai-zu-cheng-yuan"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 静态组成员")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_23 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("赋予组成员 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, ".static"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 类，可将组成员设置成静态，这样它就不会根据宽度而变化了。")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_24 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, ".static"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("类继承于 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "group-item"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 类之下。")])], -1);

var groupvue_type_template_id_3db1df1c_hoisted_25 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-group merge indent full>\n    <hl-button type=\"primary\" outline class=\"static\">Static</hl-button>\n    <hl-button type=\"primary\" outline>Auto</hl-button>\n    <hl-button type=\"primary\" outline>Auto</hl-button>\n    <hl-button type=\"primary\" outline class=\"static\">Static</hl-button>\n</hl-group>\n\n<hl-group dir=\"vertical\" merge indent full class=\"m-t-lg\" style=\"height: 200px;\">\n    <hl-button type=\"primary\" outline class=\"static\">Static</hl-button>\n    <hl-button type=\"primary\" outline>Auto</hl-button>\n    <hl-button type=\"primary\" outline>Auto</hl-button>\n    <hl-button type=\"primary\" outline class=\"static\">Static</hl-button>\n</hl-group>\n")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_26 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "zu-qian-tao"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#zu-qian-tao"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 组嵌套")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_27 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "组里面还可以再嵌套组，这样可以构造更加复杂的布局。同时它们的下级节点之间还保持相互融合。使用方法如 ui > li 一样简单。", -1);

var groupvue_type_template_id_3db1df1c_hoisted_28 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-row>\n  <hl-col span=\"col-md-12\">\n    <p>横向嵌套</p>\n    <hl-group dir=\"horizontal\" merge indent full class=\"dom-area\" data-name=\"G1\">\n      <hl-group dir=\"vertical\" merge indent full class=\"dom-area\" data-name=\"G2\">\n        <span>Item</span>\n        <span>Item</span>\n        <span>Item</span>\n      </hl-group>\n      <hl-group dir=\"vertical\" merge indent full class=\"dom-area\" data-name=\"G2\">\n        <span>Item</span>\n        <span>Item</span>\n        <span>Item</span>\n      </hl-group>\n      <hl-group dir=\"vertical\" merge indent full class=\"dom-area\" data-name=\"G2\">\n        <span>Item</span>\n        <span>Item</span>\n        <span>Item</span>\n      </hl-group>\n    </hl-group>\n  </hl-col>\n  <hl-col span=\"col-md-12\">\n    <p>纵向嵌套</p>\n    <hl-group dir=\"vertical\" merge indent full class=\"dom-area\" data-name=\"G1\">\n      <hl-group dir=\"horizontal\" merge indent full class=\"dom-area\" data-name=\"G2\">\n        <span>Item</span>\n        <span>Item</span>\n        <span>Item</span>\n      </hl-group>\n      <hl-group dir=\"horizontal\" merge indent full class=\"dom-area\" data-name=\"G2\">\n        <span>Item</span>\n        <span>Item</span>\n        <span>Item</span>\n      </hl-group>\n      <hl-group dir=\"horizontal\" merge indent full class=\"dom-area\" data-name=\"G2\">\n        <span>Item</span>\n        <span>Item</span>\n        <span>Item</span>\n      </hl-group>\n    </hl-group>\n  </hl-col>\n  <hl-col span=\"col-md-12\" class=\"m-t-lg\">\n    <p>多级混合嵌套</p>\n    <hl-group dir=\"horizontal\" merge indent full class=\"dom-area\" data-name=\"G1\">\n      <hl-group dir=\"vertical\" merge indent full class=\"dom-area\" data-name=\"G2\">\n        <hl-group dir=\"horizontal\" merge indent full class=\"dom-area\" data-name=\"G3\">\n          <span>Item</span>\n          <span>Item</span>\n        </hl-group>\n        <hl-group dir=\"horizontal\" merge indent full class=\"dom-area\" data-name=\"G3\">\n          <span>Item</span>\n          <span>Item</span>\n        </hl-group>\n        <hl-group dir=\"horizontal\" merge indent full class=\"dom-area\" data-name=\"G3\">\n          <span>Item</span>\n          <span>Item</span>\n        </hl-group>\n      </hl-group>\n      <hl-group dir=\"horizontal\" merge indent full class=\"dom-area\" data-name=\"G2\">\n        <hl-group dir=\"vertical\" full merge indent class=\"dom-area\" data-name=\"G3\">\n          <span>Item</span>\n          <span>Item</span>\n        </hl-group>\n        <hl-group dir=\"vertical\" merge indent full class=\"dom-area\" data-name=\"G3\">\n          <span>Item</span>\n          <span>Item</span>\n        </hl-group>\n      </hl-group>\n    </hl-group>\n  </hl-col>\n  <hl-col span=\"col-md-12\" class=\"m-t-lg\">\n    <p>多级混合嵌套</p>\n    <hl-group dir=\"vertical\" merge indent full class=\"dom-area\" data-name=\"G1\">\n      <hl-group dir=\"horizontal\" merge indent full class=\"dom-area\" data-name=\"G2\">\n        <hl-group dir=\"vertical\" merge indent full class=\"dom-area\" data-name=\"G3\">\n          <span>Item</span>\n          <span>Item</span>\n        </hl-group>\n        <hl-group dir=\"vertical\" merge indent full class=\"dom-area\" data-name=\"G3\">\n          <span>Item</span>\n          <span>Item</span>\n        </hl-group>\n        <hl-group dir=\"vertical\" merge indent full class=\"dom-area\" data-name=\"G3\">\n          <span>Item</span>\n          <span>Item</span>\n        </hl-group>\n      </hl-group>\n      <hl-group dir=\"horizontal\" merge indent full class=\"dom-area\" data-name=\"G2\">\n        <span>Item</span>\n        <span>Item</span>\n        <span>Item</span>\n      </hl-group>\n    </hl-group>\n  </hl-col>\n</hl-row>\n")], -1);

var groupvue_type_template_id_3db1df1c_hoisted_29 = /*#__PURE__*/Object(vue_esm_browser["m" /* createStaticVNode */])("<h3 id=\"shu-xing\"><a class=\"header-anchor\" href=\"#shu-xing\"></a> 属性</h3><table><thead><tr><th>参数</th><th>说明</th><th>类型</th><th>可选值</th><th>默认值</th></tr></thead><tbody><tr><td>dir</td><td>方向</td><td>string</td><td>horizontal / vertical</td><td>horizontal</td></tr><tr><td>merge</td><td>融合</td><td>boolean</td><td>-</td><td>false</td></tr><tr><td>indent</td><td>边框缩进</td><td>boolean / string</td><td>自定义缩进尺寸</td><td>false</td></tr><tr><td>full</td><td>拉伸</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>static</td><td>静态</td><td>boolean</td><td>—</td><td>false</td></tr></tbody></table>", 2);

function groupvue_type_template_id_3db1df1c_render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_hl_demo0 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo0");

  var _component_demo_block = Object(vue_esm_browser["P" /* resolveComponent */])("demo-block");

  var _component_hl_demo1 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo1");

  var _component_hl_demo2 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo2");

  var _component_hl_demo3 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo3");

  var _component_hl_demo4 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo4");

  var _component_hl_demo5 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo5");

  var _component_hl_demo6 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo6");

  var _component_right_nav = Object(vue_esm_browser["P" /* resolveComponent */])("right-nav");

  return Object(vue_esm_browser["G" /* openBlock */])(), Object(vue_esm_browser["j" /* createBlock */])("section", groupvue_type_template_id_3db1df1c_hoisted_1, [Object(vue_esm_browser["o" /* createVNode */])("div", groupvue_type_template_id_3db1df1c_hoisted_2, [groupvue_type_template_id_3db1df1c_hoisted_3, groupvue_type_template_id_3db1df1c_hoisted_4, groupvue_type_template_id_3db1df1c_hoisted_5, groupvue_type_template_id_3db1df1c_hoisted_6, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo0)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [groupvue_type_template_id_3db1df1c_hoisted_8];
    }),
    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [groupvue_type_template_id_3db1df1c_hoisted_7];
    }),
    _: 1
  }), groupvue_type_template_id_3db1df1c_hoisted_9, groupvue_type_template_id_3db1df1c_hoisted_10, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo1)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [groupvue_type_template_id_3db1df1c_hoisted_12];
    }),
    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [groupvue_type_template_id_3db1df1c_hoisted_11];
    }),
    _: 1
  }), groupvue_type_template_id_3db1df1c_hoisted_13, groupvue_type_template_id_3db1df1c_hoisted_14, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo2)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [groupvue_type_template_id_3db1df1c_hoisted_15];
    }),
    _: 1
  }), groupvue_type_template_id_3db1df1c_hoisted_16, groupvue_type_template_id_3db1df1c_hoisted_17, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo3)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [groupvue_type_template_id_3db1df1c_hoisted_18];
    }),
    _: 1
  }), groupvue_type_template_id_3db1df1c_hoisted_19, groupvue_type_template_id_3db1df1c_hoisted_20, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo4)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [groupvue_type_template_id_3db1df1c_hoisted_21];
    }),
    _: 1
  }), groupvue_type_template_id_3db1df1c_hoisted_22, groupvue_type_template_id_3db1df1c_hoisted_23, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo5)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [groupvue_type_template_id_3db1df1c_hoisted_25];
    }),
    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [groupvue_type_template_id_3db1df1c_hoisted_24];
    }),
    _: 1
  }), groupvue_type_template_id_3db1df1c_hoisted_26, groupvue_type_template_id_3db1df1c_hoisted_27, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo6)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [groupvue_type_template_id_3db1df1c_hoisted_28];
    }),
    _: 1
  }), groupvue_type_template_id_3db1df1c_hoisted_29]), Object(vue_esm_browser["o" /* createVNode */])(_component_right_nav)]);
}
// CONCATENATED MODULE: ./website/docs/zh-CN/group.md?vue&type=template&id=3db1df1c

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/extends.js
var helpers_extends = __webpack_require__(4);
var extends_default = /*#__PURE__*/__webpack_require__.n(helpers_extends);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/group.md?vue&type=script&lang=ts


/* harmony default export */ var groupvue_type_script_lang_ts = ({
  name: 'component-doc',
  components: {
    "hl-demo0": function () {
      var _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("按钮第二");

      function render(_ctx, _cache) {
        var _component_hl_input = _resolveComponent("hl-input");

        var _component_hl_button = _resolveComponent("hl-button");

        var _component_hl_group = _resolveComponent("hl-group");

        return _openBlock(), _createBlock(_component_hl_group, {
          class: "dom-area",
          "data-name": "G1"
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_input, {
              placeholder: "请输入内容",
              class: "m-r-sm"
            }), _createVNode(_component_hl_button, {
              type: "primary"
            }, {
              default: _withCtx(function () {
                return [_hoisted_1];
              }),
              _: 1
            })];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo1": function () {
      var _createVNode = vue_esm_browser["o" /* createVNode */],
          _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createVNode("p", null, "横向排列 horizontal", -1);

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_4 = /*#__PURE__*/_createVNode("p", null, "纵向排列 vertical", -1);

      var _hoisted_5 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_6 = /*#__PURE__*/_createTextVNode("Group Item");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        var _component_hl_group = _resolveComponent("hl-group");

        var _component_hl_col = _resolveComponent("hl-col");

        var _component_hl_row = _resolveComponent("hl-row");

        return _openBlock(), _createBlock(_component_hl_row, null, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_col, {
              span: "col"
            }, {
              default: _withCtx(function () {
                return [_hoisted_1, _createVNode(_component_hl_group, {
                  dir: "horizontal"
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      class: "m-r-xs"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_2];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_3];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: "col"
            }, {
              default: _withCtx(function () {
                return [_hoisted_4, _createVNode(_component_hl_group, {
                  dir: "vertical"
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      class: "m-b-xs"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_5];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_6];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            })];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo2": function () {
      var _createVNode = vue_esm_browser["o" /* createVNode */],
          _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createVNode("p", null, "横向融合", -1);

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_4 = /*#__PURE__*/_createVNode("br", null, null, -1);

      var _hoisted_5 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_6 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_7 = /*#__PURE__*/_createVNode("p", null, "纵向融合", -1);

      var _hoisted_8 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_9 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_10 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_11 = /*#__PURE__*/_createTextVNode("Group Item");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        var _component_hl_group = _resolveComponent("hl-group");

        var _component_hl_col = _resolveComponent("hl-col");

        var _component_hl_row = _resolveComponent("hl-row");

        return _openBlock(), _createBlock(_component_hl_row, null, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_col, {
              span: "col"
            }, {
              default: _withCtx(function () {
                return [_hoisted_1, _createVNode(_component_hl_group, {
                  dir: "horizontal",
                  merge: ""
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      class: "m-r-xs"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_2];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_3];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                }), _hoisted_4, _createVNode(_component_hl_group, {
                  dir: "horizontal",
                  merge: "",
                  class: "m-t-lg"
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      outline: "",
                      class: "m-r-xs"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_5];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_6];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: "col"
            }, {
              default: _withCtx(function () {
                return [_hoisted_7, _createVNode(_component_hl_group, {
                  dir: "vertical",
                  merge: ""
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      class: "m-b-xs"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_8];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_9];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                }), _createVNode(_component_hl_group, {
                  dir: "vertical",
                  merge: "",
                  class: "m-l-lg"
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      outline: "",
                      class: "m-b-xs"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_10];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_11];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            })];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo3": function () {
      var _createVNode = vue_esm_browser["o" /* createVNode */],
          _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createVNode("p", null, "横向缩进融合", -1);

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_4 = /*#__PURE__*/_createVNode("p", null, "纵向缩进融合", -1);

      var _hoisted_5 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_6 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_7 = /*#__PURE__*/_createVNode("p", null, [/*#__PURE__*/_createTextVNode("你也可以设置具体数值"), /*#__PURE__*/_createVNode("code", null, "indent=\"-2px\""), /*#__PURE__*/_createTextVNode("来设置缩进距离")], -1);

      var _hoisted_8 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_9 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_10 = /*#__PURE__*/_createVNode("p", null, [/*#__PURE__*/_createTextVNode("正值设置"), /*#__PURE__*/_createVNode("code", null, "indent=\"1vw\"")], -1);

      var _hoisted_11 = /*#__PURE__*/_createTextVNode("Group Item");

      var _hoisted_12 = /*#__PURE__*/_createTextVNode("Group Item");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        var _component_hl_group = _resolveComponent("hl-group");

        var _component_hl_col = _resolveComponent("hl-col");

        var _component_hl_row = _resolveComponent("hl-row");

        return _openBlock(), _createBlock(_component_hl_row, null, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_col, {
              span: "col-md-12"
            }, {
              default: _withCtx(function () {
                return [_hoisted_1, _createVNode(_component_hl_group, {
                  dir: "horizontal",
                  class: "m-t-sm",
                  merge: "",
                  indent: ""
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_2];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_3];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: "col-md-12"
            }, {
              default: _withCtx(function () {
                return [_hoisted_4, _createVNode(_component_hl_group, {
                  dir: "vertical",
                  class: "m-t-sm",
                  merge: "",
                  indent: ""
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_5];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_6];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: "col",
              class: "m-t-xl"
            }, {
              default: _withCtx(function () {
                return [_hoisted_7, _createVNode(_component_hl_group, {
                  dir: "horizontal",
                  class: "m-t-sm",
                  merge: "",
                  indent: "-2px"
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      style: {
                        "border-width": "2px"
                      },
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_8];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary",
                      style: {
                        "border-width": "2px"
                      },
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_9];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: "col",
              class: "m-t-xl"
            }, {
              default: _withCtx(function () {
                return [_hoisted_10, _createVNode(_component_hl_group, {
                  dir: "horizontal",
                  class: "m-t-sm",
                  indent: "1vw"
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      style: {
                        "border-width": "2px"
                      },
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_11];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary",
                      style: {
                        "border-width": "2px"
                      },
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_12];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            })];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo4": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("Auto");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("Auto");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("Auto");

      var _hoisted_4 = /*#__PURE__*/_createTextVNode("Auto");

      var _hoisted_5 = /*#__PURE__*/_createVNode("p", null, [/*#__PURE__*/_createTextVNode("使用"), /*#__PURE__*/_createVNode("code", null, "vertical"), /*#__PURE__*/_createTextVNode("样式将组纵向排列后，会自动变为高度自适应")], -1);

      var _hoisted_6 = /*#__PURE__*/_createTextVNode("Auto");

      var _hoisted_7 = /*#__PURE__*/_createTextVNode("Auto");

      var _hoisted_8 = /*#__PURE__*/_createTextVNode("Auto");

      var _hoisted_9 = /*#__PURE__*/_createTextVNode("Auto");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        var _component_hl_group = _resolveComponent("hl-group");

        var _component_hl_col = _resolveComponent("hl-col");

        var _component_hl_row = _resolveComponent("hl-row");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_row, null, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_col, {
              span: "col"
            }, {
              default: _withCtx(function () {
                return [_createVNode(_component_hl_group, {
                  full: ""
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      class: "m-r-sm",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_1];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_2];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: "col"
            }, {
              default: _withCtx(function () {
                return [_createVNode(_component_hl_group, {
                  merge: "",
                  indent: "",
                  full: ""
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      class: "m-r-sm",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_3];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_4];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            })];
          }),
          _: 1
        }), _hoisted_5, _createVNode(_component_hl_row, {
          class: "m-t-md"
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_col, {
              span: "col"
            }, {
              default: _withCtx(function () {
                return [_createVNode(_component_hl_group, {
                  dir: "vertical",
                  full: "",
                  style: {
                    "height": "120px"
                  }
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      class: "m-b-sm",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_6];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_7];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: "col"
            }, {
              default: _withCtx(function () {
                return [_createVNode(_component_hl_group, {
                  dir: "vertical",
                  merge: "",
                  indent: "",
                  full: "",
                  style: {
                    "height": "120px"
                  }
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_button, {
                      type: "primary",
                      class: "m-b-sm",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_8];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_button, {
                      type: "primary",
                      outline: ""
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_9];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            })];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo5": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("Static");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("Auto");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("Auto");

      var _hoisted_4 = /*#__PURE__*/_createTextVNode("Static");

      var _hoisted_5 = /*#__PURE__*/_createTextVNode("Static");

      var _hoisted_6 = /*#__PURE__*/_createTextVNode("Auto");

      var _hoisted_7 = /*#__PURE__*/_createTextVNode("Auto");

      var _hoisted_8 = /*#__PURE__*/_createTextVNode("Static");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        var _component_hl_group = _resolveComponent("hl-group");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_group, {
          merge: "",
          indent: "",
          full: ""
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_button, {
              type: "primary",
              outline: "",
              class: "static"
            }, {
              default: _withCtx(function () {
                return [_hoisted_1];
              }),
              _: 1
            }), _createVNode(_component_hl_button, {
              type: "primary",
              outline: ""
            }, {
              default: _withCtx(function () {
                return [_hoisted_2];
              }),
              _: 1
            }), _createVNode(_component_hl_button, {
              type: "primary",
              outline: ""
            }, {
              default: _withCtx(function () {
                return [_hoisted_3];
              }),
              _: 1
            }), _createVNode(_component_hl_button, {
              type: "primary",
              outline: "",
              class: "static"
            }, {
              default: _withCtx(function () {
                return [_hoisted_4];
              }),
              _: 1
            })];
          }),
          _: 1
        }), _createVNode(_component_hl_group, {
          dir: "vertical",
          merge: "",
          indent: "",
          full: "",
          class: "m-t-lg",
          style: {
            "height": "200px"
          }
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_button, {
              type: "primary",
              outline: "",
              class: "static"
            }, {
              default: _withCtx(function () {
                return [_hoisted_5];
              }),
              _: 1
            }), _createVNode(_component_hl_button, {
              type: "primary",
              outline: ""
            }, {
              default: _withCtx(function () {
                return [_hoisted_6];
              }),
              _: 1
            }), _createVNode(_component_hl_button, {
              type: "primary",
              outline: ""
            }, {
              default: _withCtx(function () {
                return [_hoisted_7];
              }),
              _: 1
            }), _createVNode(_component_hl_button, {
              type: "primary",
              outline: "",
              class: "static"
            }, {
              default: _withCtx(function () {
                return [_hoisted_8];
              }),
              _: 1
            })];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo6": function () {
      var _createVNode = vue_esm_browser["o" /* createVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createVNode("p", null, "横向嵌套", -1);

      var _hoisted_2 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_3 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_4 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_5 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_6 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_7 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_8 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_9 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_10 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_11 = /*#__PURE__*/_createVNode("p", null, "纵向嵌套", -1);

      var _hoisted_12 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_13 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_14 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_15 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_16 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_17 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_18 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_19 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_20 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_21 = /*#__PURE__*/_createVNode("p", null, "多级混合嵌套", -1);

      var _hoisted_22 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_23 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_24 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_25 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_26 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_27 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_28 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_29 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_30 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_31 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_32 = /*#__PURE__*/_createVNode("p", null, "多级混合嵌套", -1);

      var _hoisted_33 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_34 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_35 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_36 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_37 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_38 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_39 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_40 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      var _hoisted_41 = /*#__PURE__*/_createVNode("span", null, "Item", -1);

      function render(_ctx, _cache) {
        var _component_hl_group = _resolveComponent("hl-group");

        var _component_hl_col = _resolveComponent("hl-col");

        var _component_hl_row = _resolveComponent("hl-row");

        return _openBlock(), _createBlock(_component_hl_row, null, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_col, {
              span: "col-md-12"
            }, {
              default: _withCtx(function () {
                return [_hoisted_1, _createVNode(_component_hl_group, {
                  dir: "horizontal",
                  merge: "",
                  indent: "",
                  full: "",
                  class: "dom-area",
                  "data-name": "G1"
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_group, {
                      dir: "vertical",
                      merge: "",
                      indent: "",
                      full: "",
                      class: "dom-area",
                      "data-name": "G2"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_2, _hoisted_3, _hoisted_4];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_group, {
                      dir: "vertical",
                      merge: "",
                      indent: "",
                      full: "",
                      class: "dom-area",
                      "data-name": "G2"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_5, _hoisted_6, _hoisted_7];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_group, {
                      dir: "vertical",
                      merge: "",
                      indent: "",
                      full: "",
                      class: "dom-area",
                      "data-name": "G2"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_8, _hoisted_9, _hoisted_10];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: "col-md-12"
            }, {
              default: _withCtx(function () {
                return [_hoisted_11, _createVNode(_component_hl_group, {
                  dir: "vertical",
                  merge: "",
                  indent: "",
                  full: "",
                  class: "dom-area",
                  "data-name": "G1"
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_group, {
                      dir: "horizontal",
                      merge: "",
                      indent: "",
                      full: "",
                      class: "dom-area",
                      "data-name": "G2"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_12, _hoisted_13, _hoisted_14];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_group, {
                      dir: "horizontal",
                      merge: "",
                      indent: "",
                      full: "",
                      class: "dom-area",
                      "data-name": "G2"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_15, _hoisted_16, _hoisted_17];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_group, {
                      dir: "horizontal",
                      merge: "",
                      indent: "",
                      full: "",
                      class: "dom-area",
                      "data-name": "G2"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_18, _hoisted_19, _hoisted_20];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: "col-md-12",
              class: "m-t-lg"
            }, {
              default: _withCtx(function () {
                return [_hoisted_21, _createVNode(_component_hl_group, {
                  dir: "horizontal",
                  merge: "",
                  indent: "",
                  full: "",
                  class: "dom-area",
                  "data-name": "G1"
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_group, {
                      dir: "vertical",
                      merge: "",
                      indent: "",
                      full: "",
                      class: "dom-area",
                      "data-name": "G2"
                    }, {
                      default: _withCtx(function () {
                        return [_createVNode(_component_hl_group, {
                          dir: "horizontal",
                          merge: "",
                          indent: "",
                          full: "",
                          class: "dom-area",
                          "data-name": "G3"
                        }, {
                          default: _withCtx(function () {
                            return [_hoisted_22, _hoisted_23];
                          }),
                          _: 1
                        }), _createVNode(_component_hl_group, {
                          dir: "horizontal",
                          merge: "",
                          indent: "",
                          full: "",
                          class: "dom-area",
                          "data-name": "G3"
                        }, {
                          default: _withCtx(function () {
                            return [_hoisted_24, _hoisted_25];
                          }),
                          _: 1
                        }), _createVNode(_component_hl_group, {
                          dir: "horizontal",
                          merge: "",
                          indent: "",
                          full: "",
                          class: "dom-area",
                          "data-name": "G3"
                        }, {
                          default: _withCtx(function () {
                            return [_hoisted_26, _hoisted_27];
                          }),
                          _: 1
                        })];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_group, {
                      dir: "horizontal",
                      merge: "",
                      indent: "",
                      full: "",
                      class: "dom-area",
                      "data-name": "G2"
                    }, {
                      default: _withCtx(function () {
                        return [_createVNode(_component_hl_group, {
                          dir: "vertical",
                          full: "",
                          merge: "",
                          indent: "",
                          class: "dom-area",
                          "data-name": "G3"
                        }, {
                          default: _withCtx(function () {
                            return [_hoisted_28, _hoisted_29];
                          }),
                          _: 1
                        }), _createVNode(_component_hl_group, {
                          dir: "vertical",
                          merge: "",
                          indent: "",
                          full: "",
                          class: "dom-area",
                          "data-name": "G3"
                        }, {
                          default: _withCtx(function () {
                            return [_hoisted_30, _hoisted_31];
                          }),
                          _: 1
                        })];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: "col-md-12",
              class: "m-t-lg"
            }, {
              default: _withCtx(function () {
                return [_hoisted_32, _createVNode(_component_hl_group, {
                  dir: "vertical",
                  merge: "",
                  indent: "",
                  full: "",
                  class: "dom-area",
                  "data-name": "G1"
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_group, {
                      dir: "horizontal",
                      merge: "",
                      indent: "",
                      full: "",
                      class: "dom-area",
                      "data-name": "G2"
                    }, {
                      default: _withCtx(function () {
                        return [_createVNode(_component_hl_group, {
                          dir: "vertical",
                          merge: "",
                          indent: "",
                          full: "",
                          class: "dom-area",
                          "data-name": "G3"
                        }, {
                          default: _withCtx(function () {
                            return [_hoisted_33, _hoisted_34];
                          }),
                          _: 1
                        }), _createVNode(_component_hl_group, {
                          dir: "vertical",
                          merge: "",
                          indent: "",
                          full: "",
                          class: "dom-area",
                          "data-name": "G3"
                        }, {
                          default: _withCtx(function () {
                            return [_hoisted_35, _hoisted_36];
                          }),
                          _: 1
                        }), _createVNode(_component_hl_group, {
                          dir: "vertical",
                          merge: "",
                          indent: "",
                          full: "",
                          class: "dom-area",
                          "data-name": "G3"
                        }, {
                          default: _withCtx(function () {
                            return [_hoisted_37, _hoisted_38];
                          }),
                          _: 1
                        })];
                      }),
                      _: 1
                    }), _createVNode(_component_hl_group, {
                      dir: "horizontal",
                      merge: "",
                      indent: "",
                      full: "",
                      class: "dom-area",
                      "data-name": "G2"
                    }, {
                      default: _withCtx(function () {
                        return [_hoisted_39, _hoisted_40, _hoisted_41];
                      }),
                      _: 1
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            })];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }()
  }
});
// CONCATENATED MODULE: ./website/docs/zh-CN/group.md?vue&type=script&lang=ts
 
// CONCATENATED MODULE: ./website/docs/zh-CN/group.md



groupvue_type_script_lang_ts.render = groupvue_type_template_id_3db1df1c_render

/* harmony default export */ var group = __webpack_exports__["default"] = (groupvue_type_script_lang_ts);

/***/ })

}]);