(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ 471:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/drawer.md?vue&type=template&id=94fc3c8c

var _hoisted_1 = {
  class: "doc-main-content"
};

var _hoisted_2 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", {
  class: "doc-content"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "drawer-chou-ti"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#drawer-chou-ti"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" Drawer 抽屉")]), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "请参考Dialog组件")], -1);

function render(_ctx, _cache) {
  var _component_right_nav = Object(vue_esm_browser["P" /* resolveComponent */])("right-nav");

  return Object(vue_esm_browser["G" /* openBlock */])(), Object(vue_esm_browser["j" /* createBlock */])("section", _hoisted_1, [_hoisted_2, Object(vue_esm_browser["o" /* createVNode */])(_component_right_nav)]);
}
// CONCATENATED MODULE: ./website/docs/zh-CN/drawer.md?vue&type=template&id=94fc3c8c

// CONCATENATED MODULE: ./website/docs/zh-CN/drawer.md

const script = {}
script.render = render

/* harmony default export */ var drawer = __webpack_exports__["default"] = (script);

/***/ })

}]);