(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[34],{

/***/ 429:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/button.md?vue&type=template&id=acae667e

var buttonvue_type_template_id_acae667e_hoisted_1 = {
  class: "doc-main-content"
};
var buttonvue_type_template_id_acae667e_hoisted_2 = {
  class: "doc-content"
};

var buttonvue_type_template_id_acae667e_hoisted_3 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h1", null, "Button 按钮", -1);

var buttonvue_type_template_id_acae667e_hoisted_4 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("按钮组件也拥有统一的状态色，如："), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "primary"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(", "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "danger"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 等。它继承在"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "btn"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("之下。")], -1);

var buttonvue_type_template_id_acae667e_hoisted_5 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "\n<hl-button>默认</hl-button>\n<hl-button type=\"primary\">首选</hl-button>\n<hl-button type=\"success\">成功</hl-button>\n<hl-button type=\"warning\">警告</hl-button>\n<hl-button type=\"danger\">危险</hl-button>\n<hl-button type=\"info\">信息</hl-button>\n<hl-button type=\"link\">link</hl-button>\n\n")], -1);

var buttonvue_type_template_id_acae667e_hoisted_6 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "dan-se-an-niu"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#dan-se-an-niu"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 淡色按钮")], -1);

var buttonvue_type_template_id_acae667e_hoisted_7 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("当按钮附有状态色的情况下，你可以在加上 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "effect = \"light\""), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 属性即可使它的颜色减淡。")], -1);

var _hoisted_8 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "  <hl-button effect=\"light\">默认</hl-button>\n  <hl-button type=\"primary\" effect=\"light\">首选</hl-button>\n  <hl-button type=\"success\" effect=\"light\">成功</hl-button>\n  <hl-button type=\"warning\" effect=\"light\">警告</hl-button>\n  <hl-button type=\"danger\" effect=\"light\">危险</hl-button>\n  <hl-button type=\"info\" effect=\"light\">信息</hl-button>\n")], -1);

var _hoisted_9 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "wu-tian-chong-an-niu"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#wu-tian-chong-an-niu"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 无填充按钮")], -1);

var _hoisted_10 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("当按钮附有状态色的情况下，你可以在后面加上 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "no-fill"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 属性即可使它的颜色减淡。")], -1);

var _hoisted_11 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "  <hl-button no-fill>默认</hl-button>\n  <hl-button type=\"primary\" no-fill>首选</hl-button>\n  <hl-button type=\"success\" no-fill>成功</hl-button>\n  <hl-button type=\"warning\" no-fill>警告</hl-button>\n  <hl-button type=\"danger\" no-fill>危险</hl-button>\n  <hl-button type=\"info\" no-fill>信息</hl-button>\n")], -1);

var _hoisted_12 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "an-niu-chi-cun"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#an-niu-chi-cun"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 按钮尺寸")], -1);

var _hoisted_13 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "Button 组件提供除了默认值以外的三种尺寸，可以在不同场景下选择合适的按钮尺寸。", -1);

var _hoisted_14 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-button size=\"sm\">小按钮 sm</hl-button>\n<hl-button size=\"md\">中按钮 md</hl-button>\n<hl-button size=\"lg\">大按钮 lg</hl-button>\n")], -1);

var _hoisted_15 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "an-niu-xing-tai"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#an-niu-xing-tai"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 按钮形态")], -1);

var _hoisted_16 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "按钮可以设置不同的形态以满足不同的使用场景。", -1);

var _hoisted_17 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-button type=\"info\" equal>正方</hl-button>\n<hl-button type=\"primary\" equal round>正圆</hl-button>\n<hl-button type=\"warning\" round>椭圆</hl-button>\n<hl-button type=\"primary\" outline>圆角轮廓</hl-button>\n<hl-button type=\"danger\" round outline>椭圆轮廓</hl-button>\n")], -1);

var _hoisted_18 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "jin-yong-zhuang-tai"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#jin-yong-zhuang-tai"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 禁用状态")], -1);

var _hoisted_19 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("你可以使用"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "disabled"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("属性来定义按钮是否可用，它接受一个"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "Boolean"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("值。")], -1);

var _hoisted_20 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-button disabled>Disabled</hl-button>\n<hl-button type=\"primary\" disabled>Disabled</hl-button>\n<hl-button type=\"warning\" disabled>Disabled</hl-button>\n<hl-button type=\"primary\" disabled>Disabled</hl-button>\n<hl-button type=\"danger\" disabled>Disabled</hl-button>\n<hl-button type=\"info\" disabled>Disabled</hl-button>\n<hl-button type=\"primary\" outline disabled>Disabled</hl-button>\n")], -1);

var _hoisted_21 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "tu-biao-xiu-shi"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#tu-biao-xiu-shi"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 图标修饰")], -1);

var _hoisted_22 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("按钮内置了"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "icon"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("组件，你只需要设置"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "icon=\"name\""), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("属性即可引入icon组件的图标集。"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "icon-position"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("属性可以设置图标的位置。")], -1);

var _hoisted_23 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("传入自定义图标时，请为图标加上"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "hl-icon"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("类名，这样可使得图标与按钮之间更为和谐。")])], -1);

var _hoisted_24 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-button type=\"primary\" icon=\"edit\" icon-position=\"left\">编辑内容</hl-button>\n<hl-button type=\"danger\" icon=\"search\" icon-position=\"right\">搜索</hl-button>\n<hl-button type=\"primary\" icon=\"search\" equal></hl-button>\n<hl-button type=\"primary\" icon=\"delete\" equal round no-fill></hl-button>\n<hl-button type=\"primary\" icon=\"search\" icon-position=\"right\" equal round effect=\"light\"></hl-button>\n\n<hl-button type=\"primary\" icon-position=\"left\">\n    <i class=\"hl-icon\">\n        <svg viewBox=\"0 0 14 14\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">\n            <g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\n               <path d=\"M3.81085186,7.8741634 C3.08368425,8.51551024 2.625,9.4541894 2.625,10.5 L2.625,13.475 C2.625,13.6606894 2.66115115,13.8379353 2.72679927,14.0000835 L0.525,14 C0.235050506,14 -2.25375274e-14,13.7649495 -2.25375274e-14,13.475 L-2.25375274e-14,10.5 C-2.25375274e-14,9.05025253 1.17525253,7.875 2.625,7.875 L3.81085186,7.8741634 Z M5.25,0 C5.51327888,0 5.76976817,0.0290696479 6.01643329,0.0841743439 C5.01529025,0.886619755 4.375,2.11853598 4.375,3.5 C4.375,4.88146402 5.01529025,6.11338024 6.01529372,6.91517165 C5.76976817,6.97093035 5.51327888,7 5.25,7 C3.31700338,7 1.75,5.43299662 1.75,3.5 C1.75,1.56700338 3.31700338,0 5.25,0 Z\" opacity=\"0.319999993\"></path>\n               <path d=\"M11.375,7.875 C12.8247475,7.875 14,9.05025253 14,10.5 L14,13.475 C14,13.7649495 13.7649495,14 13.475,14 L4.025,14 C3.73505051,14 3.5,13.7649495 3.5,13.475 L3.5,10.5 C3.5,9.05025253 4.67525253,7.875 6.125,7.875 L11.375,7.875 Z M8.75,0 C10.6829966,0 12.25,1.56700338 12.25,3.5 C12.25,5.43299662 10.6829966,7 8.75,7 C6.81700338,7 5.25,5.43299662 5.25,3.5 C5.25,1.56700338 6.81700338,0 8.75,0 Z\"></path>\n            </g>\n        </svg>\n    </i>\n    <span>自定义图标</span>\n</hl-button>\n\n<hl-button type=\"danger\" icon-position=\"right\">\n    <i class=\"hl-icon\">\n        <svg t=\"1611553326339\" viewBox=\"0 0 1024 1024\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M454.656 544.064c14.08 0 25.6 11.52 25.6 25.6v198.4a224 224 0 1 1-224-224h198.4z m-198.4-512A224 224 0 0 1 479.872 243.84l0.384 12.288v198.4a25.6 25.6 0 0 1-20.48 25.088l-5.12 0.512h-198.4a224 224 0 1 1 0-448z m512 0a224 224 0 0 1 0 448h-198.4a25.6 25.6 0 0 1-25.6-25.6v-198.4a224 224 0 0 1 224-224z\"></path><path d=\"M768.256 992.064a224 224 0 0 0 0-448h-198.4a25.6 25.6 0 0 0-25.6 25.6v198.4a224 224 0 0 0 224 224z\" opacity=\".32\"></path></svg>\n    </i>\n    <span>自定义图标</span>\n</hl-button>\n")], -1);

var _hoisted_25 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "tu-biao-dui-qi-fang-shi"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#tu-biao-dui-qi-fang-shi"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 图标对齐方式")], -1);

var _hoisted_26 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "rational"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 属性能使按钮图标和文字的排列方式更为合理。")], -1);

var _hoisted_27 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-row>\n  <hl-col :span=\"24\">\n    <hl-button type=\"primary\" icon=\"arrowLeft\" icon-position=\"left\" style=\"width:200px\">Icon Left</hl-button>\n    <hl-button type=\"primary\" icon=\"arrowRight\" icon-position=\"right\" style=\"width:200px\">Icon Right</hl-button>\n  </hl-col>\n  <hl-col :span=\"24\" class=\"m-t-md\">\n    <hl-button type=\"primary\" icon=\"arrowLeft\" icon-position=\"left\" style=\"width:200px\" rational>Icon Left</hl-button>\n    <hl-button type=\"primary\" icon=\"arrowRight\" icon-position=\"right\" style=\"width:200px\" rational>Icon Right</hl-button>\n  </hl-col>\n</hl-row>\n\n")], -1);

var _hoisted_28 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "an-niu-zu"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#an-niu-zu"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 按钮组")], -1);

var _hoisted_29 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("使用 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "<hl-group>"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 容器，来构建一个按钮组。")], -1);

var _hoisted_30 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("更多的分组组功能，请参考"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "<hl-group>"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("组件文档。")])], -1);

var _hoisted_31 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-group merge indent>\n  <hl-button type=\"primary\" icon=\"ChevronLeft\">上一页</hl-button>\n  <hl-button type=\"primary\" icon=\"ChevronRight\" icon-position=\"right\">下一页</hl-button>\n</hl-group>\n<hl-group merge indent>\n  <hl-button type=\"primary\" icon=\"edit\"></hl-button>\n  <hl-button type=\"primary\" icon=\"share\"></hl-button>\n  <hl-button type=\"primary\" icon=\"delete\"></hl-button>\n</hl-group>\n<hl-group merge indent>\n  <hl-button type=\"primary\" outline>上一页</hl-button>\n  <hl-button type=\"primary\" outline>下一页</hl-button>\n</hl-group>\n")], -1);

var _hoisted_32 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "yu-biao-dan-yuan-su-rong-he"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#yu-biao-dan-yuan-su-rong-he"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 与表单元素融合")], -1);

var _hoisted_33 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "group"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 容器不仅可以在按钮之间融合样式，还可以与其他表单元素相互融合。")], -1);

var _hoisted_34 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-group merge indent>\n    <hl-input v-model=\"input\" placeholder=\"请输入内容\" round></hl-input>\n    <hl-button icon=\"search\" icon-position=\"right\" round>搜索</hl-button>\n</hl-group>\n\n<hl-group merge indent>\n    <hl-select v-model=\"value\" placeholder=\"请选择\" class=\"fill\">\n    <template v-for=\"item in options\">\n      <hl-option v-if=\"item.divider\" divider>\n      </hl-option>\n      <hl-option v-else :key=\"item.value\" :label=\"item.label\" :value=\"item.value\">\n      </hl-option>\n    </template>\n    </hl-select>\n    <hl-button type=\"primary\">确定</hl-button>\n</hl-group>\n<script>\n  export default {\n    data() {\n      return {\n        input: '',\n        options: [{\n          value: '选项1',\n          label: '标记状态'\n        }, {\n          value: '选项2',\n          label: '生成报表'\n        }, {\n          value: '选项3',\n          label: '统计分析数据'\n        }, {\n          divider: true\n        }, {\n          value: '选项4',\n          label: '移动到…'\n        }, {\n          value: '选项5',\n          label: '删除'\n        }],\n        value: '',\n      }\n    }\n  }\n</script>\n")], -1);

var _hoisted_35 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "jia-zai-zhuang-tai"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#jia-zai-zhuang-tai"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 加载状态")], -1);

var _hoisted_36 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "点击按钮后进行数据加载操作，在按钮上显示加载状态。", -1);

var _hoisted_37 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("要设置为 loading 状态，只要设置"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "loading"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("属性为"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "true"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("即可。")])], -1);

var _hoisted_38 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-button type=\"primary\" :loading=\"true\">加载中</hl-button>\n<hl-button type=\"warning\" :loading=\"true\">加载中</hl-button>\n<hl-button type=\"warning\" :loading=\"true\" outline>加载中</hl-button>\n")], -1);

var _hoisted_39 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "la-shen-an-niu"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#la-shen-an-niu"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 拉伸按钮")], -1);

var _hoisted_40 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "使用block属性可以把按钮拉伸至与父元素等宽", -1);

var _hoisted_41 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-button type=\"primary\" block>拉伸至父容器宽</hl-button>\n")], -1);

var _hoisted_42 = /*#__PURE__*/Object(vue_esm_browser["m" /* createStaticVNode */])("<h3 id=\"attributes\"><a class=\"header-anchor\" href=\"#attributes\"></a> Attributes</h3><table><thead><tr><th>参数</th><th>说明</th><th>类型</th><th>可选值</th><th>默认值</th></tr></thead><tbody><tr><td>size</td><td>尺寸</td><td>string</td><td>sm / md / lg</td><td>—</td></tr><tr><td>type</td><td>类型</td><td>string</td><td>primary / success / warning / danger / info / link</td><td>—</td></tr><tr><td>equal</td><td>是否方形按钮</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>round</td><td>是否圆边按钮</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>loading</td><td>是否加载中状态</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>effect</td><td>主题颜色</td><td>string</td><td>light</td><td>-</td></tr><tr><td>no-fill</td><td>无填充背景色</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>block</td><td>是否拉伸至父元素100%的宽度</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>disabled</td><td>是否禁用状态</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>icon</td><td>图标类名</td><td>string</td><td>—</td><td>—</td></tr><tr><td>icon-position</td><td>图标的位置</td><td>string</td><td>left / right</td><td>—</td></tr><tr><td>autofocus</td><td>是否默认聚焦</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>native-type</td><td>原生 type 属性</td><td>string</td><td>button / submit / reset</td><td>button</td></tr></tbody></table>", 2);

function buttonvue_type_template_id_acae667e_render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_hl_demo0 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo0");

  var _component_demo_block = Object(vue_esm_browser["P" /* resolveComponent */])("demo-block");

  var _component_hl_demo1 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo1");

  var _component_hl_demo2 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo2");

  var _component_hl_demo3 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo3");

  var _component_hl_demo4 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo4");

  var _component_hl_demo5 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo5");

  var _component_hl_demo6 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo6");

  var _component_hl_demo7 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo7");

  var _component_hl_demo8 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo8");

  var _component_hl_demo9 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo9");

  var _component_hl_demo10 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo10");

  var _component_hl_demo11 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo11");

  var _component_right_nav = Object(vue_esm_browser["P" /* resolveComponent */])("right-nav");

  return Object(vue_esm_browser["G" /* openBlock */])(), Object(vue_esm_browser["j" /* createBlock */])("section", buttonvue_type_template_id_acae667e_hoisted_1, [Object(vue_esm_browser["o" /* createVNode */])("div", buttonvue_type_template_id_acae667e_hoisted_2, [buttonvue_type_template_id_acae667e_hoisted_3, buttonvue_type_template_id_acae667e_hoisted_4, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo0)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [buttonvue_type_template_id_acae667e_hoisted_5];
    }),
    _: 1
  }), buttonvue_type_template_id_acae667e_hoisted_6, buttonvue_type_template_id_acae667e_hoisted_7, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo1)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_8];
    }),
    _: 1
  }), _hoisted_9, _hoisted_10, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo2)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_11];
    }),
    _: 1
  }), _hoisted_12, _hoisted_13, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo3)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_14];
    }),
    _: 1
  }), _hoisted_15, _hoisted_16, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo4)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_17];
    }),
    _: 1
  }), _hoisted_18, _hoisted_19, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo5)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_20];
    }),
    _: 1
  }), _hoisted_21, _hoisted_22, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo6)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_24];
    }),
    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_23];
    }),
    _: 1
  }), _hoisted_25, _hoisted_26, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo7)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_27];
    }),
    _: 1
  }), _hoisted_28, _hoisted_29, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo8)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_31];
    }),
    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_30];
    }),
    _: 1
  }), _hoisted_32, _hoisted_33, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo9)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_34];
    }),
    _: 1
  }), _hoisted_35, _hoisted_36, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo10)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_38];
    }),
    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_37];
    }),
    _: 1
  }), _hoisted_39, _hoisted_40, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo11)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_41];
    }),
    _: 1
  }), _hoisted_42]), Object(vue_esm_browser["o" /* createVNode */])(_component_right_nav)]);
}
// CONCATENATED MODULE: ./website/docs/zh-CN/button.md?vue&type=template&id=acae667e

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/extends.js
var helpers_extends = __webpack_require__(4);
var extends_default = /*#__PURE__*/__webpack_require__.n(helpers_extends);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/button.md?vue&type=script&lang=ts


/* harmony default export */ var buttonvue_type_script_lang_ts = ({
  name: 'component-doc',
  components: {
    "hl-demo0": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("默认");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("首选");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("成功");

      var _hoisted_4 = /*#__PURE__*/_createTextVNode("警告");

      var _hoisted_5 = /*#__PURE__*/_createTextVNode("危险");

      var _hoisted_6 = /*#__PURE__*/_createTextVNode("信息");

      var _hoisted_7 = /*#__PURE__*/_createTextVNode("link");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_button, null, {
          default: _withCtx(function () {
            return [_hoisted_1];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "primary"
        }, {
          default: _withCtx(function () {
            return [_hoisted_2];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "success"
        }, {
          default: _withCtx(function () {
            return [_hoisted_3];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "warning"
        }, {
          default: _withCtx(function () {
            return [_hoisted_4];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "danger"
        }, {
          default: _withCtx(function () {
            return [_hoisted_5];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "info"
        }, {
          default: _withCtx(function () {
            return [_hoisted_6];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "link"
        }, {
          default: _withCtx(function () {
            return [_hoisted_7];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo1": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("默认");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("首选");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("成功");

      var _hoisted_4 = /*#__PURE__*/_createTextVNode("警告");

      var _hoisted_5 = /*#__PURE__*/_createTextVNode("危险");

      var _hoisted_6 = /*#__PURE__*/_createTextVNode("信息");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_button, {
          effect: "light"
        }, {
          default: _withCtx(function () {
            return [_hoisted_1];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "primary",
          effect: "light"
        }, {
          default: _withCtx(function () {
            return [_hoisted_2];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "success",
          effect: "light"
        }, {
          default: _withCtx(function () {
            return [_hoisted_3];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "warning",
          effect: "light"
        }, {
          default: _withCtx(function () {
            return [_hoisted_4];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "danger",
          effect: "light"
        }, {
          default: _withCtx(function () {
            return [_hoisted_5];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "info",
          effect: "light"
        }, {
          default: _withCtx(function () {
            return [_hoisted_6];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo2": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("默认");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("首选");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("成功");

      var _hoisted_4 = /*#__PURE__*/_createTextVNode("警告");

      var _hoisted_5 = /*#__PURE__*/_createTextVNode("危险");

      var _hoisted_6 = /*#__PURE__*/_createTextVNode("信息");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_button, {
          "no-fill": ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_1];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "primary",
          "no-fill": ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_2];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "success",
          "no-fill": ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_3];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "warning",
          "no-fill": ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_4];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "danger",
          "no-fill": ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_5];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "info",
          "no-fill": ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_6];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo3": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("小按钮 sm");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("中按钮 md");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("大按钮 lg");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_button, {
          size: "sm"
        }, {
          default: _withCtx(function () {
            return [_hoisted_1];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          size: "md"
        }, {
          default: _withCtx(function () {
            return [_hoisted_2];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          size: "lg"
        }, {
          default: _withCtx(function () {
            return [_hoisted_3];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo4": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("正方");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("正圆");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("椭圆");

      var _hoisted_4 = /*#__PURE__*/_createTextVNode("圆角轮廓");

      var _hoisted_5 = /*#__PURE__*/_createTextVNode("椭圆轮廓");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_button, {
          type: "info",
          equal: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_1];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "primary",
          equal: "",
          round: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_2];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "warning",
          round: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_3];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "primary",
          outline: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_4];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "danger",
          round: "",
          outline: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_5];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo5": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("Disabled");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("Disabled");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("Disabled");

      var _hoisted_4 = /*#__PURE__*/_createTextVNode("Disabled");

      var _hoisted_5 = /*#__PURE__*/_createTextVNode("Disabled");

      var _hoisted_6 = /*#__PURE__*/_createTextVNode("Disabled");

      var _hoisted_7 = /*#__PURE__*/_createTextVNode("Disabled");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_button, {
          disabled: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_1];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "primary",
          disabled: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_2];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "warning",
          disabled: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_3];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "primary",
          disabled: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_4];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "danger",
          disabled: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_5];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "info",
          disabled: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_6];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "primary",
          outline: "",
          disabled: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_7];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo6": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("编辑内容");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("搜索");

      var _hoisted_3 = /*#__PURE__*/_createVNode("i", {
        class: "hl-icon"
      }, [/*#__PURE__*/_createVNode("svg", {
        viewBox: "0 0 14 14",
        version: "1.1",
        xmlns: "http://www.w3.org/2000/svg"
      }, [/*#__PURE__*/_createVNode("g", {
        stroke: "none",
        "stroke-width": "1",
        fill: "none",
        "fill-rule": "evenodd"
      }, [/*#__PURE__*/_createVNode("path", {
        d: "M3.81085186,7.8741634 C3.08368425,8.51551024 2.625,9.4541894 2.625,10.5 L2.625,13.475 C2.625,13.6606894 2.66115115,13.8379353 2.72679927,14.0000835 L0.525,14 C0.235050506,14 -2.25375274e-14,13.7649495 -2.25375274e-14,13.475 L-2.25375274e-14,10.5 C-2.25375274e-14,9.05025253 1.17525253,7.875 2.625,7.875 L3.81085186,7.8741634 Z M5.25,0 C5.51327888,0 5.76976817,0.0290696479 6.01643329,0.0841743439 C5.01529025,0.886619755 4.375,2.11853598 4.375,3.5 C4.375,4.88146402 5.01529025,6.11338024 6.01529372,6.91517165 C5.76976817,6.97093035 5.51327888,7 5.25,7 C3.31700338,7 1.75,5.43299662 1.75,3.5 C1.75,1.56700338 3.31700338,0 5.25,0 Z",
        opacity: "0.319999993"
      }), /*#__PURE__*/_createVNode("path", {
        d: "M11.375,7.875 C12.8247475,7.875 14,9.05025253 14,10.5 L14,13.475 C14,13.7649495 13.7649495,14 13.475,14 L4.025,14 C3.73505051,14 3.5,13.7649495 3.5,13.475 L3.5,10.5 C3.5,9.05025253 4.67525253,7.875 6.125,7.875 L11.375,7.875 Z M8.75,0 C10.6829966,0 12.25,1.56700338 12.25,3.5 C12.25,5.43299662 10.6829966,7 8.75,7 C6.81700338,7 5.25,5.43299662 5.25,3.5 C5.25,1.56700338 6.81700338,0 8.75,0 Z"
      })])])], -1);

      var _hoisted_4 = /*#__PURE__*/_createVNode("span", null, "自定义图标", -1);

      var _hoisted_5 = /*#__PURE__*/_createVNode("i", {
        class: "hl-icon"
      }, [/*#__PURE__*/_createVNode("svg", {
        t: "1611553326339",
        viewBox: "0 0 1024 1024",
        version: "1.1",
        xmlns: "http://www.w3.org/2000/svg"
      }, [/*#__PURE__*/_createVNode("path", {
        d: "M454.656 544.064c14.08 0 25.6 11.52 25.6 25.6v198.4a224 224 0 1 1-224-224h198.4z m-198.4-512A224 224 0 0 1 479.872 243.84l0.384 12.288v198.4a25.6 25.6 0 0 1-20.48 25.088l-5.12 0.512h-198.4a224 224 0 1 1 0-448z m512 0a224 224 0 0 1 0 448h-198.4a25.6 25.6 0 0 1-25.6-25.6v-198.4a224 224 0 0 1 224-224z"
      }), /*#__PURE__*/_createVNode("path", {
        d: "M768.256 992.064a224 224 0 0 0 0-448h-198.4a25.6 25.6 0 0 0-25.6 25.6v198.4a224 224 0 0 0 224 224z",
        opacity: ".32"
      })])], -1);

      var _hoisted_6 = /*#__PURE__*/_createVNode("span", null, "自定义图标", -1);

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_button, {
          type: "primary",
          icon: "edit",
          "icon-position": "left"
        }, {
          default: _withCtx(function () {
            return [_hoisted_1];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "danger",
          icon: "search",
          "icon-position": "right"
        }, {
          default: _withCtx(function () {
            return [_hoisted_2];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "primary",
          icon: "search",
          equal: ""
        }), _createVNode(_component_hl_button, {
          type: "primary",
          icon: "delete",
          equal: "",
          round: "",
          "no-fill": ""
        }), _createVNode(_component_hl_button, {
          type: "primary",
          icon: "search",
          "icon-position": "right",
          equal: "",
          round: "",
          effect: "light"
        }), _createVNode(_component_hl_button, {
          type: "primary",
          "icon-position": "left"
        }, {
          default: _withCtx(function () {
            return [_hoisted_3, _hoisted_4];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "danger",
          "icon-position": "right"
        }, {
          default: _withCtx(function () {
            return [_hoisted_5, _hoisted_6];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo7": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("Icon Left");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("Icon Right");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("Icon Left");

      var _hoisted_4 = /*#__PURE__*/_createTextVNode("Icon Right");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        var _component_hl_col = _resolveComponent("hl-col");

        var _component_hl_row = _resolveComponent("hl-row");

        return _openBlock(), _createBlock(_component_hl_row, null, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_col, {
              span: 24
            }, {
              default: _withCtx(function () {
                return [_createVNode(_component_hl_button, {
                  type: "primary",
                  icon: "arrowLeft",
                  "icon-position": "left",
                  style: {
                    "width": "200px"
                  }
                }, {
                  default: _withCtx(function () {
                    return [_hoisted_1];
                  }),
                  _: 1
                }), _createVNode(_component_hl_button, {
                  type: "primary",
                  icon: "arrowRight",
                  "icon-position": "right",
                  style: {
                    "width": "200px"
                  }
                }, {
                  default: _withCtx(function () {
                    return [_hoisted_2];
                  }),
                  _: 1
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: 24,
              class: "m-t-md"
            }, {
              default: _withCtx(function () {
                return [_createVNode(_component_hl_button, {
                  type: "primary",
                  icon: "arrowLeft",
                  "icon-position": "left",
                  style: {
                    "width": "200px"
                  },
                  rational: ""
                }, {
                  default: _withCtx(function () {
                    return [_hoisted_3];
                  }),
                  _: 1
                }), _createVNode(_component_hl_button, {
                  type: "primary",
                  icon: "arrowRight",
                  "icon-position": "right",
                  style: {
                    "width": "200px"
                  },
                  rational: ""
                }, {
                  default: _withCtx(function () {
                    return [_hoisted_4];
                  }),
                  _: 1
                })];
              }),
              _: 1
            })];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo8": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("上一页");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("下一页");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("上一页");

      var _hoisted_4 = /*#__PURE__*/_createTextVNode("下一页");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        var _component_hl_group = _resolveComponent("hl-group");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_group, {
          merge: "",
          indent: ""
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_button, {
              type: "primary",
              icon: "ChevronLeft"
            }, {
              default: _withCtx(function () {
                return [_hoisted_1];
              }),
              _: 1
            }), _createVNode(_component_hl_button, {
              type: "primary",
              icon: "ChevronRight",
              "icon-position": "right"
            }, {
              default: _withCtx(function () {
                return [_hoisted_2];
              }),
              _: 1
            })];
          }),
          _: 1
        }), _createVNode(_component_hl_group, {
          merge: "",
          indent: ""
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_button, {
              type: "primary",
              icon: "edit"
            }), _createVNode(_component_hl_button, {
              type: "primary",
              icon: "share"
            }), _createVNode(_component_hl_button, {
              type: "primary",
              icon: "delete"
            })];
          }),
          _: 1
        }), _createVNode(_component_hl_group, {
          merge: "",
          indent: ""
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_button, {
              type: "primary",
              outline: ""
            }, {
              default: _withCtx(function () {
                return [_hoisted_3];
              }),
              _: 1
            }), _createVNode(_component_hl_button, {
              type: "primary",
              outline: ""
            }, {
              default: _withCtx(function () {
                return [_hoisted_4];
              }),
              _: 1
            })];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo9": function () {
      var _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _renderList = vue_esm_browser["N" /* renderList */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */],
          _createCommentVNode = vue_esm_browser["k" /* createCommentVNode */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("搜索");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("确定");

      function render(_ctx, _cache) {
        var _component_hl_input = _resolveComponent("hl-input");

        var _component_hl_button = _resolveComponent("hl-button");

        var _component_hl_group = _resolveComponent("hl-group");

        var _component_hl_option = _resolveComponent("hl-option");

        var _component_hl_select = _resolveComponent("hl-select");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_group, {
          merge: "",
          indent: ""
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_input, {
              modelValue: _ctx.input,
              "onUpdate:modelValue": _cache[1] || (_cache[1] = function ($event) {
                return _ctx.input = $event;
              }),
              placeholder: "请输入内容",
              round: ""
            }, null, 8, ["modelValue"]), _createVNode(_component_hl_button, {
              icon: "search",
              "icon-position": "right",
              round: ""
            }, {
              default: _withCtx(function () {
                return [_hoisted_1];
              }),
              _: 1
            })];
          }),
          _: 1
        }), _createVNode(_component_hl_group, {
          merge: "",
          indent: ""
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_select, {
              modelValue: _ctx.value,
              "onUpdate:modelValue": _cache[2] || (_cache[2] = function ($event) {
                return _ctx.value = $event;
              }),
              placeholder: "请选择",
              class: "fill"
            }, {
              default: _withCtx(function () {
                return [(_openBlock(true), _createBlock(_Fragment, null, _renderList(_ctx.options, function (item) {
                  return _openBlock(), _createBlock(_Fragment, null, [item.divider ? (_openBlock(), _createBlock(_component_hl_option, {
                    key: 0,
                    divider: ""
                  })) : (_openBlock(), _createBlock(_component_hl_option, {
                    key: item.value,
                    label: item.label,
                    value: item.value
                  }, null, 8, ["label", "value"]))], 64);
                }), 256))];
              }),
              _: 1
            }, 8, ["modelValue"]), _createVNode(_component_hl_button, {
              type: "primary"
            }, {
              default: _withCtx(function () {
                return [_hoisted_2];
              }),
              _: 1
            })];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {
        data: function data() {
          return {
            input: '',
            options: [{
              value: '选项1',
              label: '标记状态'
            }, {
              value: '选项2',
              label: '生成报表'
            }, {
              value: '选项3',
              label: '统计分析数据'
            }, {
              divider: true
            }, {
              value: '选项4',
              label: '移动到…'
            }, {
              value: '选项5',
              label: '删除'
            }],
            value: ''
          };
        }
      };
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo10": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("加载中");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("加载中");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("加载中");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_button, {
          type: "primary",
          loading: true
        }, {
          default: _withCtx(function () {
            return [_hoisted_1];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "warning",
          loading: true
        }, {
          default: _withCtx(function () {
            return [_hoisted_2];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "warning",
          loading: true,
          outline: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_3];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo11": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("拉伸至父容器宽");

      function render(_ctx, _cache) {
        var _component_hl_button = _resolveComponent("hl-button");

        return _openBlock(), _createBlock(_component_hl_button, {
          type: "primary",
          block: ""
        }, {
          default: _withCtx(function () {
            return [_hoisted_1];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }()
  }
});
// CONCATENATED MODULE: ./website/docs/zh-CN/button.md?vue&type=script&lang=ts
 
// CONCATENATED MODULE: ./website/docs/zh-CN/button.md



buttonvue_type_script_lang_ts.render = buttonvue_type_template_id_acae667e_render

/* harmony default export */ var zh_CN_button = __webpack_exports__["default"] = (buttonvue_type_script_lang_ts);

/***/ })

}]);