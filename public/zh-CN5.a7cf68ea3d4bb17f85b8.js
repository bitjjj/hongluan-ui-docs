(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[57],{

/***/ 420:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// EXTERNAL MODULE: ./website/assets/images/logo.svg
var logo = __webpack_require__(79);

// CONCATENATED MODULE: ./website/assets/images/logo-sim.svg
/* harmony default export */ var logo_sim = ("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMzhweCIgaGVpZ2h0PSIzOHB4IiB2aWV3Qm94PSIwIDAgMzggMzgiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8dGl0bGU+55S75p2/PC90aXRsZT4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCB4MT0iNTAlIiB5MT0iMCUiIHgyPSI1MCUiIHkyPSIxMDAlIiBpZD0ibGluZWFyR3JhZGllbnQtMSI+CiAgICAgICAgICAgIDxzdG9wIHN0b3AtY29sb3I9IiM3QTY2RkYiIG9mZnNldD0iMCUiPjwvc3RvcD4KICAgICAgICAgICAgPHN0b3Agc3RvcC1jb2xvcj0iIzQ0MzZGRiIgb2Zmc2V0PSIxMDAlIj48L3N0b3A+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgICAgICA8bGluZWFyR3JhZGllbnQgeDE9IjUwJSIgeTE9IjAlIiB4Mj0iNTAlIiB5Mj0iMTAwJSIgaWQ9ImxpbmVhckdyYWRpZW50LTIiPgogICAgICAgICAgICA8c3RvcCBzdG9wLWNvbG9yPSIjM0FFNEUwIiBvZmZzZXQ9IjAlIj48L3N0b3A+CiAgICAgICAgICAgIDxzdG9wIHN0b3AtY29sb3I9IiMxQkM1QkQiIG9mZnNldD0iMTAwJSI+PC9zdG9wPgogICAgICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8L2RlZnM+CiAgICA8ZyBpZD0i55S75p2/IiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBvcGFjaXR5PSIwLjgwMDAwMDAxMiI+CiAgICAgICAgPGcgaWQ9ImxvZ28tc2ltIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg2LjAwMDAwMCwgNC4wMDAwMDApIiBmaWxsLXJ1bGU9Im5vbnplcm8iPgogICAgICAgICAgICA8cGF0aCBkPSJNMjUuMjI1OTMwMywxLjY4MTc5MDYxIEwyNS4yMjU5MzAzLDI4LjMxODI3MTMgQzI1LjIyNTkzMDMsMjkuMjQ3MDY0NCAyNC40NzI5OTQ3LDMwIDIzLjU0NDIwMTYsMzAgQzIzLjE2MjMyODksMzAgMjIuNzkxODI2NCwyOS44NzAwMzQ3IDIyLjQ5MzYzNCwyOS42MzE0ODA4IEw1Ljg0NTgzMzU1LDE2LjMxMzI0MDQgQzUuMTIwNTY3OTksMTUuNzMzMDI4IDUuMDAyOTc5MjEsMTQuNjc0NzI4OSA1LjU4MzE5MTY2LDEzLjk0OTQ2MzQgQzUuNjYwNzcyNzcsMTMuODUyNDg3IDUuNzQ4ODU3MTcsMTMuNzY0NDAyNiA1Ljg0NTgzMzU1LDEzLjY4NjgyMTUgTDIyLjQ5MzYzNCwwLjM2ODU4MTEzNiBDMjMuMjE4ODk5NiwtMC4yMTE2MzEzMTcgMjQuMjc3MTk4NiwtMC4wOTQwNDI1MzYyIDI0Ljg1NzQxMTEsMC42MzEyMjMwMzEgQzI1LjA5NTk2NSwwLjkyOTQxNTQ3IDI1LjIyNTkzMDMsMS4yOTk5MTc5NiAyNS4yMjU5MzAzLDEuNjgxNzkwNjEgWiIgaWQ9Iui3r+W+hC0yIiBmaWxsPSJ1cmwoI2xpbmVhckdyYWRpZW50LTEpIj48L3BhdGg+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0wLDEuNjgxNzkwNjEgTDAsMjguMzE4MjcxMyBDMCwyOS4yNDcwNjQ0IDAuNzUyOTM1NTc4LDMwIDEuNjgxNzI4NjgsMzAgQzIuMDYzNjAxMzMsMzAgMi40MzQxMDM4MywyOS44NzAwMzQ3IDIuNzMyMjk2MjYsMjkuNjMxNDgwOCBMMTkuMzgwMDk2NywxNi4zMTMyNDA0IEMyMC4xMDUzNjIzLDE1LjczMzAyOCAyMC4yMjI5NTExLDE0LjY3NDcyODkgMTkuNjQyNzM4NiwxMy45NDk0NjM0IEMxOS41NjUxNTc1LDEzLjg1MjQ4NyAxOS40NzcwNzMxLDEzLjc2NDQwMjYgMTkuMzgwMDk2NywxMy42ODY4MjE1IEwyLjczMjI5NjI2LDAuMzY4NTgxMTM2IEMyLjAwNzAzMDcsLTAuMjExNjMxMzE3IDAuOTQ4NzMxNjYyLC0wLjA5NDA0MjUzNjIgMC4zNjg1MTkyMDksMC42MzEyMjMwMzEgQzAuMTI5OTY1MjU3LDAuOTI5NDE1NDcgMCwxLjI5OTkxNzk2IDAsMS42ODE3OTA2MSBaIiBpZD0i6Lev5b6ELTIiIGZpbGw9InVybCgjbGluZWFyR3JhZGllbnQtMikiPjwvcGF0aD4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPg==");
// CONCATENATED MODULE: ./website/assets/images/logo-light.svg
/* harmony default export */ var logo_light = ("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMTUwcHgiIGhlaWdodD0iMzhweCIgdmlld0JveD0iMCAwIDE1MCAzOCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj4KICAgIDx0aXRsZT7nlLvmnb88L3RpdGxlPgogICAgPGRlZnM+CiAgICAgICAgPGxpbmVhckdyYWRpZW50IHgxPSI1MCUiIHkxPSIwJSIgeDI9IjUwJSIgeTI9IjEwMCUiIGlkPSJsaW5lYXJHcmFkaWVudC0xIj4KICAgICAgICAgICAgPHN0b3Agc3RvcC1jb2xvcj0iIzdBNjZGRiIgb2Zmc2V0PSIwJSI+PC9zdG9wPgogICAgICAgICAgICA8c3RvcCBzdG9wLWNvbG9yPSIjNDQzNkZGIiBvZmZzZXQ9IjEwMCUiPjwvc3RvcD4KICAgICAgICA8L2xpbmVhckdyYWRpZW50PgogICAgICAgIDxsaW5lYXJHcmFkaWVudCB4MT0iNTAlIiB5MT0iMCUiIHgyPSI1MCUiIHkyPSIxMDAlIiBpZD0ibGluZWFyR3JhZGllbnQtMiI+CiAgICAgICAgICAgIDxzdG9wIHN0b3AtY29sb3I9IiMzQUU0RTAiIG9mZnNldD0iMCUiPjwvc3RvcD4KICAgICAgICAgICAgPHN0b3Agc3RvcC1jb2xvcj0iIzFCQzVCRCIgb2Zmc2V0PSIxMDAlIj48L3N0b3A+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGlkPSLnlLvmnb8iIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIiBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPgogICAgICAgIDxnIGlkPSJsb2dvLXNpbSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNi4wMDAwMDAsIDQuMDAwMDAwKSIgZmlsbC1ydWxlPSJub256ZXJvIiBvcGFjaXR5PSIwLjgwMDAwMDAxMiI+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0yNS4yMjU5MzAzLDEuNjgxNzkwNjEgTDI1LjIyNTkzMDMsMjguMzE4MjcxMyBDMjUuMjI1OTMwMywyOS4yNDcwNjQ0IDI0LjQ3Mjk5NDcsMzAgMjMuNTQ0MjAxNiwzMCBDMjMuMTYyMzI4OSwzMCAyMi43OTE4MjY0LDI5Ljg3MDAzNDcgMjIuNDkzNjM0LDI5LjYzMTQ4MDggTDUuODQ1ODMzNTUsMTYuMzEzMjQwNCBDNS4xMjA1Njc5OSwxNS43MzMwMjggNS4wMDI5NzkyMSwxNC42NzQ3Mjg5IDUuNTgzMTkxNjYsMTMuOTQ5NDYzNCBDNS42NjA3NzI3NywxMy44NTI0ODcgNS43NDg4NTcxNywxMy43NjQ0MDI2IDUuODQ1ODMzNTUsMTMuNjg2ODIxNSBMMjIuNDkzNjM0LDAuMzY4NTgxMTM2IEMyMy4yMTg4OTk2LC0wLjIxMTYzMTMxNyAyNC4yNzcxOTg2LC0wLjA5NDA0MjUzNjIgMjQuODU3NDExMSwwLjYzMTIyMzAzMSBDMjUuMDk1OTY1LDAuOTI5NDE1NDcgMjUuMjI1OTMwMywxLjI5OTkxNzk2IDI1LjIyNTkzMDMsMS42ODE3OTA2MSBaIiBpZD0i6Lev5b6ELTIiIGZpbGw9InVybCgjbGluZWFyR3JhZGllbnQtMSkiPjwvcGF0aD4KICAgICAgICAgICAgPHBhdGggZD0iTTAsMS42ODE3OTA2MSBMMCwyOC4zMTgyNzEzIEMwLDI5LjI0NzA2NDQgMC43NTI5MzU1NzgsMzAgMS42ODE3Mjg2OCwzMCBDMi4wNjM2MDEzMywzMCAyLjQzNDEwMzgzLDI5Ljg3MDAzNDcgMi43MzIyOTYyNiwyOS42MzE0ODA4IEwxOS4zODAwOTY3LDE2LjMxMzI0MDQgQzIwLjEwNTM2MjMsMTUuNzMzMDI4IDIwLjIyMjk1MTEsMTQuNjc0NzI4OSAxOS42NDI3Mzg2LDEzLjk0OTQ2MzQgQzE5LjU2NTE1NzUsMTMuODUyNDg3IDE5LjQ3NzA3MzEsMTMuNzY0NDAyNiAxOS4zODAwOTY3LDEzLjY4NjgyMTUgTDIuNzMyMjk2MjYsMC4zNjg1ODExMzYgQzIuMDA3MDMwNywtMC4yMTE2MzEzMTcgMC45NDg3MzE2NjIsLTAuMDk0MDQyNTM2MiAwLjM2ODUxOTIwOSwwLjYzMTIyMzAzMSBDMC4xMjk5NjUyNTcsMC45Mjk0MTU0NyAwLDEuMjk5OTE3OTYgMCwxLjY4MTc5MDYxIFoiIGlkPSLot6/lvoQtMiIgZmlsbD0idXJsKCNsaW5lYXJHcmFkaWVudC0yKSI+PC9wYXRoPgogICAgICAgIDwvZz4KICAgICAgICA8ZyBpZD0ibG9nbyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNi4wMDAwMDAsIDQuMDAwMDAwKSIgZmlsbC1ydWxlPSJub256ZXJvIj4KICAgICAgICAgICAgPHBhdGggZD0iTTI1LjIyNTkzMDMsMS42ODE3OTA2MSBMMjUuMjI1OTMwMywyOC4zMTgyNzEzIEMyNS4yMjU5MzAzLDI5LjI0NzA2NDQgMjQuNDcyOTk0NywzMCAyMy41NDQyMDE2LDMwIEMyMy4xNjIzMjg5LDMwIDIyLjc5MTgyNjQsMjkuODcwMDM0NyAyMi40OTM2MzQsMjkuNjMxNDgwOCBMNS44NDU4MzM1NSwxNi4zMTMyNDA0IEM1LjEyMDU2Nzk5LDE1LjczMzAyOCA1LjAwMjk3OTIxLDE0LjY3NDcyODkgNS41ODMxOTE2NiwxMy45NDk0NjM0IEM1LjY2MDc3Mjc3LDEzLjg1MjQ4NyA1Ljc0ODg1NzE3LDEzLjc2NDQwMjYgNS44NDU4MzM1NSwxMy42ODY4MjE1IEwyMi40OTM2MzQsMC4zNjg1ODExMzYgQzIzLjIxODg5OTYsLTAuMjExNjMxMzE3IDI0LjI3NzE5ODYsLTAuMDk0MDQyNTM2MiAyNC44NTc0MTExLDAuNjMxMjIzMDMxIEMyNS4wOTU5NjUsMC45Mjk0MTU0NyAyNS4yMjU5MzAzLDEuMjk5OTE3OTYgMjUuMjI1OTMwMywxLjY4MTc5MDYxIFoiIGlkPSLot6/lvoQtMiIgZmlsbD0idXJsKCNsaW5lYXJHcmFkaWVudC0xKSIgb3BhY2l0eT0iMC44MDAwMDAwMTIiPjwvcGF0aD4KICAgICAgICAgICAgPHBhdGggZD0iTTAsMS42ODE3OTA2MSBMMCwyOC4zMTgyNzEzIEMwLDI5LjI0NzA2NDQgMC43NTI5MzU1NzgsMzAgMS42ODE3Mjg2OCwzMCBDMi4wNjM2MDEzMywzMCAyLjQzNDEwMzgzLDI5Ljg3MDAzNDcgMi43MzIyOTYyNiwyOS42MzE0ODA4IEwxOS4zODAwOTY3LDE2LjMxMzI0MDQgQzIwLjEwNTM2MjMsMTUuNzMzMDI4IDIwLjIyMjk1MTEsMTQuNjc0NzI4OSAxOS42NDI3Mzg2LDEzLjk0OTQ2MzQgQzE5LjU2NTE1NzUsMTMuODUyNDg3IDE5LjQ3NzA3MzEsMTMuNzY0NDAyNiAxOS4zODAwOTY3LDEzLjY4NjgyMTUgTDIuNzMyMjk2MjYsMC4zNjg1ODExMzYgQzIuMDA3MDMwNywtMC4yMTE2MzEzMTcgMC45NDg3MzE2NjIsLTAuMDk0MDQyNTM2MiAwLjM2ODUxOTIwOSwwLjYzMTIyMzAzMSBDMC4xMjk5NjUyNTcsMC45Mjk0MTU0NyAwLDEuMjk5OTE3OTYgMCwxLjY4MTc5MDYxIFoiIGlkPSLot6/lvoQtMiIgZmlsbD0idXJsKCNsaW5lYXJHcmFkaWVudC0yKSIgb3BhY2l0eT0iMC44MDAwMDAwMTIiPjwvcGF0aD4KICAgICAgICAgICAgPHBhdGggZD0iTTUwLjg5NjYyOTQsOSBDNTMuMzc4NTU2OCw5IDU1LjQwMjkyMywxMC45NTkwNTIzIDU1LjUwNzc0OTksMTMuNDE1MTc4OCBMNTUuNTEyMDE0LDEzLjYxNTM4NDYgTDU1LjUxMjAxNCwxNi4zODQ2MTU0IEM1NS41MTIwMTQsMTguOTMzNjIxOSA1My40NDU2MzU5LDIxIDUwLjg5NjYyOTQsMjEgQzQ4LjQxNDcwMiwyMSA0Ni4zOTAzMzU4LDE5LjA0MDk0NzcgNDYuMjg1NTA5LDE2LjU4NDgyMTIgTDQ2LjI4MTI0NDgsMTYuMzg0NjE1NCBMNDYuMjgxMjQ0OCwxMy42MTUzODQ2IEM0Ni4yODEyNDQ4LDExLjA2NjM3ODEgNDguMzQ3NjIyOSw5IDUwLjg5NjYyOTQsOSBaIE03My4wNTA0NzU2LDkgQzc1LjExNzI2NTQsOSA3Ni44NjY3NTk5LDEwLjM1ODUwMjcgNzcuNDU0NzI1NSwxMi4yMzEyNzQ2IEw3NS40NDkzODYzLDEyLjIzMTAzNSBDNzQuOTcwNjExMSwxMS40MDMxNjM1IDc0LjA3NTU5MTYsMTAuODQ2MTUzOCA3My4wNTA0NzU2LDEwLjg0NjE1MzggQzcxLjU3NTY5MzIsMTAuODQ2MTUzOCA3MC4zNzAxNzMyLDExLjk5OTAwMyA3MC4yODU5NDU3LDEzLjQ1MjY3MTMgTDcwLjI4MTI0NDgsMTMuNjE1Mzg0NiBMNzAuMjgxMjQ0OCwxNi4zODQ2MTU0IEM3MC4yODEyNDQ4LDE3LjkxNDAxOTMgNzEuNTIxMDcxNiwxOS4xNTM4NDYyIDczLjA1MDQ3NTYsMTkuMTUzODQ2MiBDNzQuNTI1MjU3OSwxOS4xNTM4NDYyIDc1LjczMDc3NzksMTguMDAwOTk3IDc1LjgxNTAwNTQsMTYuNTQ3MzI4NyBMNzUuODE5NzA2MywxNi4zODQ2MTU0IEw3NS44MTk3MDYzLDE1LjkyMzA3NjkgTDczLjk3MzU1MjUsMTUuOTIzMDc2OSBDNzMuNDYzNzUxMiwxNS45MjMwNzY5IDczLjA1MDQ3NTYsMTUuNTA5ODAxMyA3My4wNTA0NzU2LDE1IEw3My4wNTA0NzU2LDE0LjA3NjkyMzEgTDc3LjY2NTg2MDIsMTQuMDc2OTIzMSBMNzcuNjY1ODYwMiwxNi4zODQ2MTU0IEM3Ny42NjU4NjAyLDE4LjkzMzYyMTkgNzUuNTk5NDgyMSwyMSA3My4wNTA0NzU2LDIxIEM3MC41Njg1NDgxLDIxIDY4LjU0NDE4MTksMTkuMDQwOTQ3NyA2OC40MzkzNTUxLDE2LjU4NDgyMTIgTDY4LjQzNTA5MDksMTYuMzg0NjE1NCBMNjguNDM1MDkwOSwxMy42MTUzODQ2IEM2OC40MzUwOTA5LDExLjA2NjM3ODEgNzAuNTAxNDY5LDkgNzMuMDUwNDc1Niw5IFogTTg5LjY2NTg2MDIsOS40NjE1Mzg0NiBMODkuNjY1ODYwMiwxNi4zODQ2MTU0IEM4OS42NjU4NjAyLDE3LjkxNDAxOTMgOTAuOTA1Njg3LDE5LjE1Mzg0NjIgOTIuNDM1MDkwOSwxOS4xNTM4NDYyIEM5My45MDk4NzMzLDE5LjE1Mzg0NjIgOTUuMTE1MzkzMywxOC4wMDA5OTcgOTUuMTk5NjIwOCwxNi41NDczMjg3IEw5NS4yMDQzMjE3LDE2LjM4NDYxNTQgTDk1LjIwNDMyMTcsOS40NjE1Mzg0NiBMOTcuMDUwNDc1Niw5LjQ2MTUzODQ2IEw5Ny4wNTA0NzU2LDE2LjM4NDYxNTQgQzk3LjA1MDQ3NTYsMTguOTMzNjIxOSA5NC45ODQwOTc1LDIxIDkyLjQzNTA5MDksMjEgQzg5Ljk1MzE2MzUsMjEgODcuOTI4Nzk3MywxOS4wNDA5NDc3IDg3LjgyMzk3MDUsMTYuNTg0ODIxMiBMODcuODE5NzA2MywxNi4zODQ2MTU0IEw4Ny44MTk3MDYzLDkuNDYxNTM4NDYgTDg5LjY2NTg2MDIsOS40NjE1Mzg0NiBaIE0xMjguNDM1MDkxLDkuNDYxNTM4NDYgTDEyOC40MzUwOTEsMTYuMzg0NjE1NCBDMTI4LjQzNTA5MSwxNy45MTQwMTkzIDEyOS42NzQ5MTgsMTkuMTUzODQ2MiAxMzEuMjA0MzIyLDE5LjE1Mzg0NjIgQzEzMi42NzkxMDQsMTkuMTUzODQ2MiAxMzMuODg0NjI0LDE4LjAwMDk5NyAxMzMuOTY4ODUyLDE2LjU0NzMyODcgTDEzMy45NzM1NTIsMTYuMzg0NjE1NCBMMTMzLjk3MzU1Miw5LjQ2MTUzODQ2IEwxMzUuODE5NzA2LDkuNDYxNTM4NDYgTDEzNS44MTk3MDYsMTYuMzg0NjE1NCBDMTM1LjgxOTcwNiwxOC45MzM2MjE5IDEzMy43NTMzMjgsMjEgMTMxLjIwNDMyMiwyMSBDMTI4LjcyMjM5NCwyMSAxMjYuNjk4MDI4LDE5LjA0MDk0NzcgMTI2LjU5MzIwMSwxNi41ODQ4MjEyIEwxMjYuNTg4OTM3LDE2LjM4NDYxNTQgTDEyNi41ODg5MzcsOS40NjE1Mzg0NiBMMTI4LjQzNTA5MSw5LjQ2MTUzODQ2IFogTTEyMi40MzUwOTEsMTguMzMxODU0OCBDMTIzLjA0Njg1MywxOC4zMzE4NTQ4IDEyMy41NDI3ODMsMTguODI3Nzg1NSAxMjMuNTQyNzgzLDE5LjQzOTU0NzEgQzEyMy41NDI3ODMsMjAuMDUxMzA4NiAxMjMuMDQ2ODUzLDIwLjU0NzIzOTQgMTIyLjQzNTA5MSwyMC41NDcyMzk0IEMxMjEuODIzMzI5LDIwLjU0NzIzOTQgMTIxLjMyNzM5OSwyMC4wNTEzMDg2IDEyMS4zMjczOTksMTkuNDM5NTQ3MSBDMTIxLjMyNzM5OSwxOC44Mjc3ODU1IDEyMS44MjMzMjksMTguMzMxODU0OCAxMjIuNDM1MDkxLDE4LjMzMTg1NDggWiBNMzcuMDUwNDc1Niw5LjQ2MTUzODQ2IEwzNy4wNTA0NzU2LDE0LjA3NjkyMzEgTDQyLjU4ODkzNzEsMTQuMDc2OTIzMSBMNDIuNTg4OTM3MSw5LjQ2MTUzODQ2IEw0NC40MzUwOTA5LDkuNDYxNTM4NDYgTDQ0LjQzNTA5MDksMTkuNjE1Mzg0NiBDNDQuNDM1MDkwOSwyMC4xMjUxODU5IDQ0LjAyMTgxNTMsMjAuNTM4NDYxNSA0My41MTIwMTQsMjAuNTM4NDYxNSBMNDIuNTg4OTM3MSwyMC41Mzg0NjE1IEw0Mi41ODg5MzcxLDE1LjkyMzA3NjkgTDM3LjA1MDQ3NTYsMTUuOTIzMDc2OSBMMzcuMDUwNDc1NiwxOS42MTUzODQ2IEMzNy4wNTA0NzU2LDIwLjEyNTE4NTkgMzYuNjM3MTk5OSwyMC41Mzg0NjE1IDM2LjEyNzM5ODYsMjAuNTM4NDYxNSBMMzUuMjA0MzIxNywyMC41Mzg0NjE1IEwzNS4yMDQzMjE3LDkuNDYxNTM4NDYgTDM3LjA1MDQ3NTYsOS40NjE1Mzg0NiBaIE01OS4yMDQzMjE3LDkuNDYxNTM4NDYgTDY0Ljc0Mjc4MzIsMTcuMzExMjcyOSBMNjQuNzQyNzgzMiw5LjQ2MTUzODQ2IEw2Ni41ODg5MzcxLDkuNDYxNTM4NDYgTDY2LjU4ODkzNzEsMTkuNjE1Mzg0NiBDNjYuNTg4OTM3MSwyMC4xMjUxODU5IDY2LjE3NTY2MTUsMjAuNTM4NDYxNSA2NS42NjU4NjAyLDIwLjUzODQ2MTUgTDY0Ljc0Mjc4MzIsMjAuNTM4NDYxNSBMNTkuMjA0MzIxNywxMi42OTQ1NDI0IEw1OS4yMDQzMjE3LDE5LjYxNTM4NDYgQzU5LjIwNDMyMTcsMjAuMTI1MTg1OSA1OC43OTEwNDYxLDIwLjUzODQ2MTUgNTguMjgxMjQ0OCwyMC41Mzg0NjE1IEw1Ny4zNTgxNjc5LDIwLjUzODQ2MTUgTDU3LjM1ODE2NzksOS40NjE1Mzg0NiBMNTkuMjA0MzIxNyw5LjQ2MTUzODQ2IFogTTExMC44OTY2MjksOS40NjE1Mzg0NiBMMTE2LjQzNTA5MSwxNy4zMTEyNzI5IEwxMTYuNDM1MDkxLDkuNDYxNTM4NDYgTDExOC4yODEyNDUsOS40NjE1Mzg0NiBMMTE4LjI4MTI0NSwxOS42MTUzODQ2IEMxMTguMjgxMjQ1LDIwLjEyNTE4NTkgMTE3Ljg2Nzk2OSwyMC41Mzg0NjE1IDExNy4zNTgxNjgsMjAuNTM4NDYxNSBMMTE2LjQzNTA5MSwyMC41Mzg0NjE1IEwxMTAuODk2NjI5LDEyLjY5NDU0MjQgTDExMC44OTY2MjksMTkuNjE1Mzg0NiBDMTEwLjg5NjYyOSwyMC4xMjUxODU5IDExMC40ODMzNTQsMjAuNTM4NDYxNSAxMDkuOTczNTUyLDIwLjUzODQ2MTUgTDEwOS4wNTA0NzYsMjAuNTM4NDYxNSBMMTA5LjA1MDQ3Niw5LjQ2MTUzODQ2IEwxMTAuODk2NjI5LDkuNDYxNTM4NDYgWiBNODEuMzU4MTY3OSw5LjQ2MTUzODQ2IEw4MS4zNTgxNjc5LDE4LjY5MjMwNzcgTDg2Ljg5NjYyOTQsMTguNjkyMzA3NyBMODYuODk2NjI5NCwxOS42MTUzODQ2IEM4Ni44OTY2Mjk0LDIwLjEyNTE4NTkgODYuNDgzMzUzOCwyMC41Mzg0NjE1IDg1Ljk3MzU1MjUsMjAuNTM4NDYxNSBMNzkuNTEyMDE0LDIwLjUzODQ2MTUgTDc5LjUxMjAxNCw5LjQ2MTUzODQ2IEw4MS4zNTgxNjc5LDkuNDYxNTM4NDYgWiBNMTM5LjUxMjAxNCw5LjQ2MTUzODQ2IEwxMzkuNTEyMDE0LDE5LjYxNTM4NDYgQzEzOS41MTIwMTQsMjAuMTI1MTg1OSAxMzkuMDk4NzM4LDIwLjUzODQ2MTUgMTM4LjU4ODkzNywyMC41Mzg0NjE1IEwxMzcuNjY1ODYsMjAuNTM4NDYxNSBMMTM3LjY2NTg2LDkuNDYxNTM4NDYgTDEzOS41MTIwMTQsOS40NjE1Mzg0NiBaIE0xMDMuNTEyMDE0LDkuNDYxNTM4NDYgTDEwNy4yMDQzMjIsMjAuNTM4NDYxNSBMMTA1LjM1ODE2OCwyMC41Mzg0NjE1IEwxMDQuNDM1MDkxLDE3Ljc2OTIzMDggTDEwMC43NDI3ODMsMTcuNzY5MjMwOCBMOTkuODE5NzA2MywyMC41Mzg0NjE1IEw5Ny45NzM1NTI1LDIwLjUzODQ2MTUgTDEwMS42NjU4Niw5LjQ2MTUzODQ2IEwxMDMuNTEyMDE0LDkuNDYxNTM4NDYgWiBNNTAuODk2NjI5NCwxMC44NDYxNTM4IEM0OS40MjE4NDcsMTAuODQ2MTUzOCA0OC4yMTYzMjcxLDExLjk5OTAwMyA0OC4xMzIwOTk2LDEzLjQ1MjY3MTMgTDQ4LjEyNzM5ODYsMTMuNjE1Mzg0NiBMNDguMTI3Mzk4NiwxNi4zODQ2MTU0IEM0OC4xMjczOTg2LDE3LjkxNDAxOTMgNDkuMzY3MjI1NSwxOS4xNTM4NDYyIDUwLjg5NjYyOTQsMTkuMTUzODQ2MiBDNTIuMzcxNDExOCwxOS4xNTM4NDYyIDUzLjU3NjkzMTgsMTguMDAwOTk3IDUzLjY2MTE1OTIsMTYuNTQ3MzI4NyBMNTMuNjY1ODYwMiwxNi4zODQ2MTU0IEw1My42NjU4NjAyLDEzLjYxNTM4NDYgQzUzLjY2NTg2MDIsMTIuMDg1OTgwNyA1Mi40MjYwMzMzLDEwLjg0NjE1MzggNTAuODk2NjI5NCwxMC44NDYxNTM4IFogTTEwMi41ODg5MzcsMTIuMjMwNzY5MiBMMTAxLjM1ODQ3NiwxNS45MjMwNzY5IEwxMDMuODE5Mzk5LDE1LjkyMzA3NjkgTDEwMi41ODg5MzcsMTIuMjMwNzY5MiBaIiBpZD0i5b2i54q2IiBmaWxsPSIjRkZGRkZGIj48L3BhdGg+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=");
// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--11-0!./website/pages/layout.vue?vue&type=template&id=201675ce




var _hoisted_1 = {
  class: "bg-stripes"
};

var _hoisted_2 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", {
  class: "logo"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("img", {
  src: logo["a" /* default */],
  alt: "hongluang.ui"
})], -1);

var _hoisted_3 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", {
  class: "logo-simple"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("img", {
  src: logo_sim,
  alt: "hongluang.ui"
})], -1);

var _hoisted_4 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", {
  class: "header-left"
}, " Smarter UI framework ", -1);

var _hoisted_5 = {
  class: "header-right"
};
var _hoisted_6 = {
  class: "userinfo"
};

var _hoisted_7 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("span", {
  class: "name"
}, "Hi，Song")], -1);

var _hoisted_8 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("span", null, "朱松", -1);

var _hoisted_9 = {
  class: "page-title"
};

var _hoisted_10 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h5", null, "页面标题", -1);

var _hoisted_11 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", {
  class: "hl-breadcrumb"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", {
  class: "hl-breadcrumb-item",
  "data-separator": "•"
}, "基础功能"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", {
  class: "hl-breadcrumb-item",
  "data-separator": "•"
}, "Layout布局")], -1);

var _hoisted_12 = {
  class: "sub-header-right"
};

var _hoisted_13 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("Button");

var _hoisted_14 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("数据流");

var _hoisted_15 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("传输属性值");

var _hoisted_16 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("已断线");

var _hoisted_17 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("待恢复");

var _hoisted_18 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("其他");

var _hoisted_19 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", {
  class: "aside-logo"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("img", {
  src: logo_light,
  alt: "hongluang.ui"
})], -1);

var _hoisted_20 = {
  class: "aside-menu"
};

var _hoisted_21 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("span", null, "基础功能", -1);

var _hoisted_22 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("Grid 栅格系统");

var _hoisted_23 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("Layout 布局");

var _hoisted_24 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("Typography 排版");

var _hoisted_25 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("Color 色彩");

var _hoisted_26 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("Icon 图标");

var _hoisted_27 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("Group 组");

var _hoisted_28 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("Panel 面板");

var _hoisted_29 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("Scrollbar 滚动条");

var _hoisted_30 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("表单类组件");

var _hoisted_31 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("数据类");

var _hoisted_32 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("消息通知");

var _hoisted_33 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("导航菜单");

var _hoisted_34 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("其他");

var _hoisted_35 = {
  class: "aside-tools"
};

var _hoisted_36 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", {
  class: "table-tools",
  style: {
    "flex-grow": "1"
  }
}, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 已选中 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("span", {
  class: "text-danger font-bold"
}, "3"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 条，共 180 条 ")], -1);

var _hoisted_37 = {
  class: "panel-header-right"
};

var _hoisted_38 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("新建");

var _hoisted_39 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", {
  style: {
    "height": "1440px"
  }
}, null, -1);

var _hoisted_40 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", {
  class: "footer-left"
}, " 2021 © hongluang-ui ", -1);

var _hoisted_41 = {
  class: "footer-right"
};

var _hoisted_42 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("Button");

var _hoisted_43 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("Button");

var _hoisted_44 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h4", null, "自定义对话框title", -1);

var _hoisted_45 = {
  class: "dialog-footer"
};

var _hoisted_46 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("取 消");

var _hoisted_47 = /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("确 定");

function render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_hl_button = Object(vue_esm_browser["P" /* resolveComponent */])("hl-button");

  var _component_hl_group = Object(vue_esm_browser["P" /* resolveComponent */])("hl-group");

  var _component_hl_badge = Object(vue_esm_browser["P" /* resolveComponent */])("hl-badge");

  var _component_hl_thumb = Object(vue_esm_browser["P" /* resolveComponent */])("hl-thumb");

  var _component_hl_header = Object(vue_esm_browser["P" /* resolveComponent */])("hl-header");

  var _component_hl_dropdown_item = Object(vue_esm_browser["P" /* resolveComponent */])("hl-dropdown-item");

  var _component_hl_dropdown_menu = Object(vue_esm_browser["P" /* resolveComponent */])("hl-dropdown-menu");

  var _component_hl_dropdown = Object(vue_esm_browser["P" /* resolveComponent */])("hl-dropdown");

  var _component_hl_sub_header = Object(vue_esm_browser["P" /* resolveComponent */])("hl-sub-header");

  var _component_hl_icon = Object(vue_esm_browser["P" /* resolveComponent */])("hl-icon");

  var _component_hl_menu_item = Object(vue_esm_browser["P" /* resolveComponent */])("hl-menu-item");

  var _component_hl_submenu = Object(vue_esm_browser["P" /* resolveComponent */])("hl-submenu");

  var _component_hl_menu = Object(vue_esm_browser["P" /* resolveComponent */])("hl-menu");

  var _component_hl_scrollbar = Object(vue_esm_browser["P" /* resolveComponent */])("hl-scrollbar");

  var _component_hl_aside = Object(vue_esm_browser["P" /* resolveComponent */])("hl-aside");

  var _component_hl_panel = Object(vue_esm_browser["P" /* resolveComponent */])("hl-panel");

  var _component_hl_main = Object(vue_esm_browser["P" /* resolveComponent */])("hl-main");

  var _component_hl_container = Object(vue_esm_browser["P" /* resolveComponent */])("hl-container");

  var _component_hl_footer = Object(vue_esm_browser["P" /* resolveComponent */])("hl-footer");

  var _component_hl_layout = Object(vue_esm_browser["P" /* resolveComponent */])("hl-layout");

  var _component_hl_checkbox = Object(vue_esm_browser["P" /* resolveComponent */])("hl-checkbox");

  var _component_hl_checkbox_group = Object(vue_esm_browser["P" /* resolveComponent */])("hl-checkbox-group");

  var _component_hl_form_item = Object(vue_esm_browser["P" /* resolveComponent */])("hl-form-item");

  var _component_hl_form = Object(vue_esm_browser["P" /* resolveComponent */])("hl-form");

  var _component_hl_dialog = Object(vue_esm_browser["P" /* resolveComponent */])("hl-dialog");

  return Object(vue_esm_browser["G" /* openBlock */])(), Object(vue_esm_browser["j" /* createBlock */])(vue_esm_browser["b" /* Fragment */], null, [Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_1, [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_layout, {
    class: [$data.boxed, $data.header, $data.aside, $data.sub_header, $data.footer]
  }, {
    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_header, null, {
        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
          return [_hoisted_2, _hoisted_3, _hoisted_4, Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_5, [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_group, {
            indent: "var(--xs)",
            class: "m-r-xxl"
          }, {
            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
                type: "primary",
                size: "lg",
                icon: "mail",
                effect: "no-fill",
                equal: ""
              }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
                type: "primary",
                size: "lg",
                icon: "setting",
                effect: "no-fill",
                equal: ""
              }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
                type: "primary",
                size: "lg",
                icon: "Layout",
                effect: "no-fill",
                equal: "",
                onClick: _cache[1] || (_cache[1] = function ($event) {
                  return $options.btnClick('right', 'drawer');
                })
              })];
            }),
            _: 1
          }), Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_6, [_hoisted_7, Object(vue_esm_browser["o" /* createVNode */])(_component_hl_thumb, {
            radius: "",
            type: "primary",
            size: "sm",
            light: "",
            class: "face"
          }, {
            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_badge, {
                type: "danger",
                position: "rt",
                dot: ""
              }), _hoisted_8];
            }),
            _: 1
          })])])];
        }),
        _: 1
      }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_sub_header, null, {
        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
          return [Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_9, [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
            icon: "ChevronLeft",
            class: "go-back",
            equal: "",
            "no-fill": ""
          }), _hoisted_10, _hoisted_11]), Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_12, [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
            type: "primary",
            effect: "light"
          }, {
            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [_hoisted_13];
            }),
            _: 1
          }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_dropdown, {
            class: "m-l-xs"
          }, {
            dropdown: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_dropdown_menu, null, {
                default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_dropdown_item, null, {
                    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                      return [_hoisted_14];
                    }),
                    _: 1
                  }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_dropdown_item, null, {
                    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                      return [_hoisted_15];
                    }),
                    _: 1
                  }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_dropdown_item, null, {
                    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                      return [_hoisted_16];
                    }),
                    _: 1
                  }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_dropdown_item, {
                    disabled: ""
                  }, {
                    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                      return [_hoisted_17];
                    }),
                    _: 1
                  }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_dropdown_item, {
                    divider: ""
                  }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_dropdown_item, null, {
                    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                      return [_hoisted_18];
                    }),
                    _: 1
                  })];
                }),
                _: 1
              })];
            }),
            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
                type: "primary",
                class: "bg-primary light-4",
                icon: "More",
                effect: "light",
                equal: ""
              })];
            }),
            _: 1
          })])];
        }),
        _: 1
      }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_container, null, {
        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
          return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_aside, {
            class: _ctx.collapse
          }, {
            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [_hoisted_19, Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_20, [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_scrollbar, null, {
                default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu, {
                    "default-active": "1-2",
                    "unique-opened": "true",
                    onOpen: _ctx.handleOpen,
                    onClose: _ctx.handleClose
                  }, {
                    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_submenu, {
                        index: "1"
                      }, {
                        title: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                          return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_icon, {
                            name: "Computer"
                          }), _hoisted_21];
                        }),
                        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                          return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                            index: "1-1"
                          }, {
                            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                              return [_hoisted_22];
                            }),
                            _: 1
                          }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                            index: "1-2"
                          }, {
                            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                              return [_hoisted_23];
                            }),
                            _: 1
                          }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                            index: "1-3"
                          }, {
                            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                              return [_hoisted_24];
                            }),
                            _: 1
                          }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                            index: "1-4"
                          }, {
                            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                              return [_hoisted_25];
                            }),
                            _: 1
                          }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                            index: "1-5"
                          }, {
                            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                              return [_hoisted_26];
                            }),
                            _: 1
                          }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                            index: "1-5"
                          }, {
                            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                              return [_hoisted_27];
                            }),
                            _: 1
                          }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                            index: "1-5"
                          }, {
                            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                              return [_hoisted_28];
                            }),
                            _: 1
                          }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                            index: "1-5"
                          }, {
                            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                              return [_hoisted_29];
                            }),
                            _: 1
                          })];
                        }),
                        _: 1
                      }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                        index: "2"
                      }, {
                        title: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                          return [_hoisted_30];
                        }),
                        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                          return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_icon, {
                            name: "application"
                          })];
                        }),
                        _: 1
                      }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                        index: "3"
                      }, {
                        title: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                          return [_hoisted_31];
                        }),
                        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                          return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_icon, {
                            name: "server"
                          })];
                        }),
                        _: 1
                      }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                        index: "4"
                      }, {
                        title: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                          return [_hoisted_32];
                        }),
                        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                          return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_icon, {
                            name: "Message"
                          })];
                        }),
                        _: 1
                      }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                        index: "4"
                      }, {
                        title: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                          return [_hoisted_33];
                        }),
                        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                          return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_icon, {
                            name: "Treemenu"
                          })];
                        }),
                        _: 1
                      }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_menu_item, {
                        index: "4"
                      }, {
                        title: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                          return [_hoisted_34];
                        }),
                        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                          return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_icon, {
                            name: "box"
                          })];
                        }),
                        _: 1
                      })];
                    }),
                    _: 1
                  }, 8, ["onOpen", "onClose"])];
                }),
                _: 1
              })]), Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_35, [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_group, {
                dir: "horizontal",
                full: ""
              }, {
                default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
                    icon: "home",
                    "no-fill": ""
                  }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
                    icon: "setting",
                    "no-fill": ""
                  }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
                    icon: "box",
                    "no-fill": ""
                  })];
                }),
                _: 1
              })])];
            }),
            _: 1
          }, 8, ["class"]), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_main, null, {
            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_panel, {
                style: {
                  "--panelRadius": "4px"
                },
                borderless: ""
              }, {
                header: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [_hoisted_36, Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_37, [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
                    type: "primary",
                    icon: "download",
                    effect: "light",
                    equal: "",
                    class: "m-l-xs"
                  }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
                    type: "primary",
                    class: "m-l-xs"
                  }, {
                    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                      return [_hoisted_38];
                    }),
                    _: 1
                  })])];
                }),
                footer: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [];
                }),
                default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [_hoisted_39];
                }),
                _: 1
              })];
            }),
            _: 1
          })];
        }),
        _: 1
      }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_footer, null, {
        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
          return [_hoisted_40, Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_41, [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_group, {
            indent: "var(--sm)"
          }, {
            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
                effect: "no-fill"
              }, {
                default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [_hoisted_42];
                }),
                _: 1
              }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
                effect: "no-fill"
              }, {
                default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [_hoisted_43];
                }),
                _: 1
              })];
            }),
            _: 1
          })])];
        }),
        _: 1
      })];
    }),
    _: 1
  }, 8, ["class"])]), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_dialog, {
    modelValue: $data.dialogVisible,
    "onUpdate:modelValue": _cache[9] || (_cache[9] = function ($event) {
      return $data.dialogVisible = $event;
    }),
    width: 400,
    "show-as": $data.showAs,
    placement: $data.placement
  }, {
    header: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_44];
    }),
    footer: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])("span", _hoisted_45, [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
        class: "m-l-sm",
        onClick: _cache[7] || (_cache[7] = function ($event) {
          return $data.dialogVisible = false;
        })
      }, {
        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
          return [_hoisted_46];
        }),
        _: 1
      }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_button, {
        type: "primary",
        class: "m-l-sm",
        onClick: _cache[8] || (_cache[8] = function ($event) {
          return $data.dialogVisible = false;
        })
      }, {
        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
          return [_hoisted_47];
        }),
        _: 1
      })])];
    }),
    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_form, {
        ref: "form",
        model: _ctx.form,
        width: ['col-24', 'col-24'],
        "label-position": ['left', 'middle']
      }, {
        default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
          return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_form_item, {
            label: "Boxed"
          }, {
            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox_group, {
                modelValue: $data.boxed,
                "onUpdate:modelValue": _cache[2] || (_cache[2] = function ($event) {
                  return $data.boxed = $event;
                })
              }, {
                default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox, {
                    label: "boxed",
                    name: "type"
                  })];
                }),
                _: 1
              }, 8, ["modelValue"])];
            }),
            _: 1
          }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_form_item, {
            label: "Aside"
          }, {
            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox_group, {
                modelValue: $data.aside,
                "onUpdate:modelValue": _cache[3] || (_cache[3] = function ($event) {
                  return $data.aside = $event;
                })
              }, {
                default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox, {
                    label: "fixed-aside",
                    name: "type"
                  }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox, {
                    label: "collapse",
                    name: "type"
                  })];
                }),
                _: 1
              }, 8, ["modelValue"])];
            }),
            _: 1
          }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_form_item, {
            label: "Header"
          }, {
            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox_group, {
                modelValue: $data.header,
                "onUpdate:modelValue": _cache[4] || (_cache[4] = function ($event) {
                  return $data.header = $event;
                })
              }, {
                default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox, {
                    label: "fixed-header",
                    name: "type"
                  }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox, {
                    label: "full-header",
                    name: "type"
                  })];
                }),
                _: 1
              }, 8, ["modelValue"])];
            }),
            _: 1
          }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_form_item, {
            label: "SubHeader"
          }, {
            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox_group, {
                modelValue: $data.sub_header,
                "onUpdate:modelValue": _cache[5] || (_cache[5] = function ($event) {
                  return $data.sub_header = $event;
                })
              }, {
                default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox, {
                    label: "fixed-sub-header",
                    name: "type"
                  }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox, {
                    label: "full-sub-header",
                    name: "type"
                  })];
                }),
                _: 1
              }, 8, ["modelValue"])];
            }),
            _: 1
          }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_form_item, {
            label: "Footer"
          }, {
            default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
              return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox_group, {
                modelValue: $data.footer,
                "onUpdate:modelValue": _cache[6] || (_cache[6] = function ($event) {
                  return $data.footer = $event;
                })
              }, {
                default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
                  return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox, {
                    label: "fixed-footer",
                    name: "type"
                  }), Object(vue_esm_browser["o" /* createVNode */])(_component_hl_checkbox, {
                    label: "full-footer",
                    name: "type"
                  })];
                }),
                _: 1
              }, 8, ["modelValue"])];
            }),
            _: 1
          })];
        }),
        _: 1
      }, 8, ["model"])];
    }),
    _: 1
  }, 8, ["modelValue", "show-as", "placement"])], 64);
}
// CONCATENATED MODULE: ./website/pages/layout.vue?vue&type=template&id=201675ce

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--11-0!./website/pages/layout.vue?vue&type=script&lang=js
/* harmony default export */ var layoutvue_type_script_lang_js = ({
  data: function data() {
    return {
      header: ['fixed-header'],
      aside: ['fixed-aside'],
      sub_header: [],
      footer: [],
      boxed: [],
      dialogVisible: false,
      showAs: 'dialog',
      placement: 'center'
    };
  },
  methods: {
    btnClick: function btnClick(placement, showAs) {
      if (showAs === void 0) {
        showAs = 'dialog';
      }

      this.placement = placement;
      this.showAs = showAs;
      this.dialogVisible = true;
    }
  }
});
// CONCATENATED MODULE: ./website/pages/layout.vue?vue&type=script&lang=js
 
// CONCATENATED MODULE: ./website/pages/layout.vue



layoutvue_type_script_lang_js.render = render

/* harmony default export */ var layout = __webpack_exports__["default"] = (layoutvue_type_script_lang_js);

/***/ })

}]);