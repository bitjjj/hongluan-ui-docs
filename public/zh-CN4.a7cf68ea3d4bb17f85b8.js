(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[56],{

/***/ 417:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./website/assets/images/theme-index-blue.png
/* harmony default export */ var theme_index_blue = (__webpack_require__.p + "static/theme-index-blue.8fbb67d.png");
// CONCATENATED MODULE: ./website/assets/images/theme-index-red.png
/* harmony default export */ var theme_index_red = (__webpack_require__.p + "static/theme-index-red.5e43266.png");
// CONCATENATED MODULE: ./website/assets/images/duohui.svg
/* harmony default export */ var duohui = ("data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iOTgiIGhlaWdodD0iMTUwIiB2aWV3Qm94PSIwIDAgOTggMTUwIiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPgo8dGl0bGU+ZHVvaHVpLWVsZW1lbnQ8L3RpdGxlPgo8ZGVzYz5DcmVhdGVkIHVzaW5nIEZpZ21hPC9kZXNjPgo8ZyBpZD0iQ2FudmFzIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMjIxNiAxNDApIj4KPGNsaXBQYXRoIGlkPSJjbGlwLTAiIGNsaXAtcnVsZT0iZXZlbm9kZCI+CjxwYXRoIGQ9Ik0gMjIxNiAtMTQwTCAyMzE0IC0xNDBMIDIzMTQgMTBMIDIyMTYgMTBMIDIyMTYgLTE0MFoiIGZpbGw9IiNGRkZGRkYiLz4KPC9jbGlwUGF0aD4KPGcgaWQ9ImR1b2h1aS1lbGVtZW50IiBjbGlwLXBhdGg9InVybCgjY2xpcC0wKSI+CjxwYXRoIGQ9Ik0gMjIxNiAtMTQwTCAyMzE0IC0xNDBMIDIzMTQgMTBMIDIyMTYgMTBMIDIyMTYgLTE0MFoiIGZpbGw9IiNGRkZGRkYiLz4KPGcgaWQ9IkR1b2h1aSBJY29uIDIiPgo8ZyBpZD0iVmVjdG9yIj4KPHVzZSB4bGluazpocmVmPSIjcGF0aDBfZmlsbCIgdHJhbnNmb3JtPSJtYXRyaXgoMS4yNjYzNiAwIDAgMS4yMTkyOSAyMjMwIC03NS43MDM4KSIgZmlsbD0iIzNBODhGRCIvPgo8L2c+CjxnIGlkPSJWZWN0b3IiPgo8dXNlIHhsaW5rOmhyZWY9IiNwYXRoMV9maWxsIiB0cmFuc2Zvcm09Im1hdHJpeCgxLjQxNTM0IDAgMCAxLjIxODM0IDIyMjYgLTExMikiIGZpbGw9IiMzNUFGRkIiLz4KPC9nPgo8L2c+CjwvZz4KPC9nPgo8ZGVmcz4KPHBhdGggaWQ9InBhdGgwX2ZpbGwiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTSAyNS43NzIyIDEuMDA2MzVDIDI2LjIgMC4zNzQ5ODMgMjYuODk0OSAtMi42NjEzZS0wNyAyNy42MzQ5IC0yLjY2MTNlLTA3QyAyOC4zNzU1IC0yLjY2MTNlLTA3IDI5LjA2OTkgMC4zNzQ5ODMgMjkuNDk4MyAxLjAwNjM1QyAzNC40MDkgOC4yNDk2NCA0Ny43OTggMjcuOTk2NCA1NC41OTg2IDM4LjAyNjJDIDU1LjM4NzggMzkuMTkwMSA1NS40ODk2IDQwLjcxNjIgNTQuODYyMyA0MS45ODNDIDU0LjIzNSA0My4yNDk4IDUyLjk4MzMgNDQuMDQ1MiA1MS42MTc4IDQ0LjA0NTJDIDM5LjY2OTQgNDQuMDQ1MiAxNS42MDEgNDQuMDQ1MiAzLjY1MjEgNDQuMDQ1MkMgMi4yODcxMiA0NC4wNDUyIDEuMDM1NDUgNDMuMjQ5OCAwLjQwODE3MyA0MS45ODNDIC0wLjIxOTEwNiA0MC43MTYyIC0wLjExNzM1MyAzOS4xOTAxIDAuNjcxODA0IDM4LjAyNjJDIDcuNDcyNDIgMjcuOTk2NCAyMC44NjA5IDguMjQ5NjQgMjUuNzcyMiAxLjAwNjM1WiIvPgo8cGF0aCBpZD0icGF0aDFfZmlsbCIgZmlsbC1ydWxlPSJldmVub2RkIiBkPSJNIDI0Ljk5MzkgMS40NDcwM0MgMjUuNjEzNyAwLjUzMzExNyAyNi41NTcyIC0zLjAxNjE0ZS0wNyAyNy41NTUxIC0zLjAxNjE0ZS0wN0MgMjguNTUzNSAtMy4wMTYxNGUtMDcgMjkuNDk3IDAuNTMzMTE3IDMwLjExNjggMS40NDcwM0MgMzUuNDY3NCA5LjMzODU0IDQ3Ljg3MjUgMjcuNjM0MiA1NC40MDQzIDM3LjI2ODFDIDU1LjE4MjUgMzguNDE1NyA1NS4zMjk5IDM5Ljk4MiA1NC43ODQ3IDQxLjI5OTlDIDU0LjIzODkgNDIuNjE4NSA1My4wOTYgNDMuNDU1NiA1MS44NDI2IDQzLjQ1NTZDIDM5LjkwMjMgNDMuNDU1NiAxNS4yMDg0IDQzLjQ1NTYgMy4yNjgxMSA0My40NTU2QyAyLjAxNDcxIDQzLjQ1NTYgMC44NzE3MzkgNDIuNjE4NSAwLjMyNTk3OCA0MS4yOTk5QyAtMC4yMTk3ODMgMzkuOTgyIC0wLjA3MTc4MTMgMzguNDE1NyAwLjcwNjM5IDM3LjI2ODFDIDcuMjM4MTggMjcuNjM0MiAxOS42NDMzIDkuMzM4NTQgMjQuOTkzOSAxLjQ0NzAzWiIvPgo8L2RlZnM+Cjwvc3ZnPgo=");
// CONCATENATED MODULE: ./website/assets/images/bit.svg
/* harmony default export */ var bit = ("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMzVweCIgaGVpZ2h0PSIyMnB4IiB2aWV3Qm94PSIwIDAgMzUgMjIiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDUxLjIgKDU3NTE5KSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5sb2dvX2JpdDwvdGl0bGU+CiAgICA8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCB4MT0iNTAlIiB5MT0iMCUiIHgyPSI1MCUiIHkyPSI5OS4wMjkwODI0JSIgaWQ9ImxpbmVhckdyYWRpZW50LTEiPgogICAgICAgICAgICA8c3RvcCBzdG9wLWNvbG9yPSIjNzMzOThEIiBvZmZzZXQ9IjAlIj48L3N0b3A+CiAgICAgICAgICAgIDxzdG9wIHN0b3AtY29sb3I9IiM1OTRBOTUiIG9mZnNldD0iMTAwJSI+PC9zdG9wPgogICAgICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICAgICAgPHBhdGggZD0iTTI2LjYxOTYzNjEsMTguMjA2OTQ3NiBDMjYuNjE5NjM2MSwyMi42NjcwMzg4IDIzLjkzMjk2NzUsMjUuNTM4NjA0NCAxOS4xMTU0OTI3LDI1LjUzODYwNDQgQzE3LjM4NjE0MjgsMjUuNTM4NjA0NCAxNS43ODAzMTc5LDI1LjIwMjU3MDEgMTQuNTQ1MDY4LDI0LjQ5OTk1MyBDMTMuMTI0NTMwNiwyMy43MDU2OTAyIDEzLjEyNDUzMDYsMjIuNjk3NTg3NCAxMy4xMjQ1MzA2LDIxLjY1ODkzNiBMMTMuMTI0NTMwNiw2LjIzMTkwODI0IEMxMy4xMjQ1MzA2LDUuMTMyMTU5NzMgMTMuODAzOTE4LDQuNTgyMjg1NDcgMTUuMTkzNTc0Miw0LjU4MjI4NTQ3IEMxNi41ODMyMzA0LDQuNTgyMjg1NDcgMTcuMjYyNjE3OCw1LjEzMjE1OTczIDE3LjI2MjYxNzgsNi4yMzE5MDgyNCBMMTcuMjYyNjE3OCwxMi4wOTcyMzM2IEMxOC4wMzQ2NDksMTEuNTc3OTA4IDE5LjA1MzczMDIsMTEuMzMzNTE5NCAyMC4yMjcyMTc3LDExLjMzMzUxOTQgQzIzLjk5NDczLDExLjMzMzUxOTQgMjYuNjE5NjM2MSwxMy44MDc5NTM2IDI2LjYxOTYzNjEsMTguMjA2OTQ3NiBaIE0xNy4yNjI2MTc4LDIxLjc4MTEzMDMgQzE3Ljc4NzU5OTEsMjIuMDg2NjE2IDE4LjQzNjEwNTMsMjIuMjM5MzU4OCAxOS4xNDYzNzQsMjIuMjM5MzU4OCBDMjEuMTUzNjU1MSwyMi4yMzkzNTg4IDIyLjM4ODkwNTEsMjAuODk1MjIxOCAyMi4zODg5MDUxLDE4LjQ4MTg4NDcgQzIyLjM4ODkwNTEsMTYuMDM3OTk5MiAyMS4xMjI3NzM5LDE0LjYzMjc2NDkgMTkuMjA4MTM2NSwxNC42MzI3NjQ5IEMxOC40NjY5ODY1LDE0LjYzMjc2NDkgMTcuNzg3NTk5MSwxNC44NDY2MDQ5IDE3LjI2MjYxNzgsMTUuMjQzNzM2MyBMMTcuMjYyNjE3OCwyMS43ODExMzAzIFogTTMzLjk2OTM3MzIsNy4zMDExMDgxOCBDMzMuOTY5MzczMiw4LjcwNjM0MjQgMzIuOTUwMjkyLDkuNjgzODk2NjMgMzEuNTI5NzU0Niw5LjY4Mzg5NjYzIEMzMC4xMDkyMTcyLDkuNjgzODk2NjMgMjkuMDkwMTM2LDguNzA2MzQyNCAyOS4wOTAxMzYsNy4zMDExMDgxOCBDMjkuMDkwMTM2LDUuODk1ODczOTcgMzAuMTA5MjE3Miw0Ljk0ODg2ODMxIDMxLjUyOTc1NDYsNC45NDg4NjgzMSBDMzIuOTUwMjkyLDQuOTQ4ODY4MzEgMzMuOTY5MzczMiw1Ljg5NTg3Mzk3IDMzLjk2OTM3MzIsNy4zMDExMDgxOCBaIE0zMy41OTg3OTgyLDEyLjk4MzE0MjIgTDMzLjU5ODc5ODIsMjMuODI3ODg0NSBDMzMuNTk4Nzk4MiwyNC45Mjc2MzMgMzIuOTE5NDEwOCwyNS40Nzc1MDcyIDMxLjUyOTc1NDYsMjUuNDc3NTA3MiBDMzAuMTQwMDk4NCwyNS40Nzc1MDcyIDI5LjQ2MDcxMDksMjQuOTI3NjMzIDI5LjQ2MDcxMDksMjMuODI3ODg0NSBMMjkuNDYwNzEwOSwxMi45ODMxNDIyIEMyOS40NjA3MTA5LDExLjg4MzM5MzcgMzAuMTQwMDk4NCwxMS4zMzM1MTk0IDMxLjUyOTc1NDYsMTEuMzMzNTE5NCBDMzIuOTE5NDEwOCwxMS4zMzM1MTk0IDMzLjU5ODc5ODIsMTEuODgzMzkzNyAzMy41OTg3OTgyLDEyLjk4MzE0MjIgWiBNNDUuNzM1MTI4OCwyMS44MTE2Nzg4IEM0Ni44Nzc3MzUsMjEuODExNjc4OCA0Ny4yMTc0Mjg3LDIzLjAwMzA3MzEgNDcuMjE3NDI4NywyMy42NzUxNDE2IEM0Ny4yMTc0Mjg3LDI0LjEwMjgyMTYgNDcuMDYzMDIyNSwyNC41MzA1MDE2IDQ2LjQ3NjI3ODgsMjQuODY2NTM1OCBDNDUuNzk2ODkxMywyNS4yNjM2NjcyIDQ0LjYyMzQwMzksMjUuNTM4NjA0NCA0My40MTkwMzUyLDI1LjUzODYwNDQgQzQxLjE5NTU4NTMsMjUuNTM4NjA0NCAzOS44Njc2OTE2LDI0LjY1MjY5NTggMzkuMTg4MzA0MiwyMy40MzA3NTMgQzM4LjU3MDY3OTIsMjIuMzMxMDA0NSAzOC41MDg5MTY3LDIxLjAxNzQxNiAzOC41MDg5MTY3LDE5LjYxMjE4MTggTDM4LjUwODkxNjcsMTUuMDI5ODk2MyBMMzcuNDI4MDczLDE1LjAyOTg5NjMgQzM2LjMxNjM0ODEsMTUuMDI5ODk2MyAzNS43NjA0ODU2LDE0LjQ4MDAyMjEgMzUuNzYwNDg1NiwxMy4zMTkxNzY0IEMzNS43NjA0ODU2LDEyLjE1ODMzMDggMzYuMzE2MzQ4MSwxMS42MDg0NTY1IDM3LjQyODA3MywxMS42MDg0NTY1IEwzOC41MDg5MTY3LDExLjYwODQ1NjUgTDM4LjUwODkxNjcsOC45MjAxODIzOCBDMzguNTA4OTE2Nyw3LjgyMDQzMzg3IDM5LjE4ODMwNDIsNy4yNzA1NTk2MSA0MC41Nzc5NjA0LDcuMjcwNTU5NjEgQzQxLjk2NzYxNjUsNy4yNzA1NTk2MSA0Mi42NDcwMDQsNy44MjA0MzM4NyA0Mi42NDcwMDQsOC45MjAxODIzOCBMNDIuNjQ3MDA0LDExLjYwODQ1NjUgTDQ1LjI0MTAyODksMTEuNjA4NDU2NSBDNDYuMzUyNzUzOCwxMS42MDg0NTY1IDQ2LjkwODYxNjMsMTIuMTU4MzMwOCA0Ni45MDg2MTYzLDEzLjMxOTE3NjQgQzQ2LjkwODYxNjMsMTQuNDgwMDIyMSA0Ni4zNTI3NTM4LDE1LjAyOTg5NjMgNDUuMjQxMDI4OSwxNS4wMjk4OTYzIEw0Mi42NDcwMDQsMTUuMDI5ODk2MyBMNDIuNjQ3MDA0LDE5LjYxMjE4MTggQzQyLjY0NzAwNCwyMC4xNjIwNTYxIDQyLjY0NzAwNCwyMC45NTYzMTg5IDQyLjkyNDkzNTIsMjEuNDc1NjQ0NiBDNDMuMTQxMTA0LDIxLjkwMzMyNDYgNDMuNTQyNTYwMiwyMi4wODY2MTYgNDQuMDY3NTQxNCwyMi4wODY2MTYgQzQ0LjQwNzIzNTEsMjIuMDg2NjE2IDQ0Ljc0NjkyODksMjEuOTk0OTcwMyA0NS4wMjQ4NjAxLDIxLjkzMzg3MzEgQzQ1LjI3MTkxMDEsMjEuODcyNzc2IDQ1LjQ4ODA3ODgsMjEuODExNjc4OCA0NS43MzUxMjg4LDIxLjgxMTY3ODggWiIgaWQ9InBhdGgtMiI+PC9wYXRoPgogICAgPC9kZWZzPgogICAgPGcgaWQ9IkRlc2lnbiIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPGcgaWQ9IlN0YXRlcyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTExNjEuMDAwMDAwLCAtMjYwLjAwMDAwMCkiPgogICAgICAgICAgICA8ZyBpZD0ibmF2IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg3NjguMDAwMDAwLCAyMjQuMDAwMDAwKSI+CiAgICAgICAgICAgICAgICA8ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSg0Ny4wMDAwMDAsIDMyLjAwMDAwMCkiIGlkPSJMb2dvcy0vLWJpdF9sb2dvLXByZXNzZWQiPgogICAgICAgICAgICAgICAgICAgIDxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDMzMy4wMDAwMDAsIDAuMDAwMDAwKSI+CiAgICAgICAgICAgICAgICAgICAgICAgIDxnIGlkPSJiaXQiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVzZSBmaWxsPSIjMTEyMjJEIiB4bGluazpocmVmPSIjcGF0aC0yIj48L3VzZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1c2UgZmlsbD0iYmxhY2siIHhsaW5rOmhyZWY9IiNwYXRoLTIiPjwvdXNlPgogICAgICAgICAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgICAgICAgICAgPC9nPgogICAgICAgICAgICAgICAgPC9nPgogICAgICAgICAgICA8L2c+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=");
// CONCATENATED MODULE: ./website/assets/images/guide.png
/* harmony default export */ var guide = (__webpack_require__.p + "static/guide.e81dcf2.png");
// CONCATENATED MODULE: ./website/assets/images/component.png
/* harmony default export */ var component = (__webpack_require__.p + "static/component.7d1ca06.png");
// CONCATENATED MODULE: ./website/assets/images/resource.png
/* harmony default export */ var resource = (__webpack_require__.p + "static/resource.53cc5d2.png");
// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--11-0!./website/pages/index.vue?vue&type=template&id=2b854c51&scoped=true









var _withId = /*#__PURE__*/Object(vue_esm_browser["ib" /* withScopeId */])("data-v-2b854c51");

Object(vue_esm_browser["J" /* pushScopeId */])("data-v-2b854c51");

var _hoisted_1 = {
  class: "banner"
};
var _hoisted_2 = {
  class: "banner-desc"
};
var _hoisted_3 = {
  ref: "indexMainImg",
  class: "jumbotron"
};

var _hoisted_4 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("img", {
  src: theme_index_blue,
  alt: ""
}, null, -1);

var _hoisted_5 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("img", {
  src: theme_index_red,
  alt: ""
}, null, -1);

var _hoisted_6 = {
  class: "sponsors"
};
var _hoisted_7 = {
  class: "sponsor",
  href: "https://www.duohui.cn/?utm_source=element&utm_medium=web&utm_campaign=element-index",
  target: "_blank"
};

var _hoisted_8 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("img", {
  width: "45",
  src: duohui,
  alt: "duohui"
}, null, -1);

var _hoisted_9 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("span", {
  class: "name"
}, "多会", -1);

var _hoisted_10 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "活动服务销售平台", -1);

var _hoisted_11 = {
  class: "sponsor",
  href: "https://bit.dev/?from=element-ui",
  target: "_blank"
};

var _hoisted_12 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("img", {
  width: "45",
  src: bit,
  alt: "bit"
}, null, -1);

var _hoisted_13 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("span", {
  class: "name"
}, "bit", -1);

var _hoisted_14 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "Share Code", -1);

var _hoisted_15 = {
  class: "cards"
};
var _hoisted_16 = {
  class: "container"
};
var _hoisted_17 = {
  class: "card"
};

var _hoisted_18 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("img", {
  src: guide,
  alt: ""
}, null, -1);

var _hoisted_19 = {
  class: "card"
};

var _hoisted_20 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("img", {
  src: component,
  alt: ""
}, null, -1);

var _hoisted_21 = {
  class: "card"
};

var _hoisted_22 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("img", {
  src: resource,
  alt: ""
}, null, -1);

Object(vue_esm_browser["H" /* popScopeId */])();

var render = /*#__PURE__*/_withId(function (_ctx, _cache, $props, $setup, $data, $options) {
  var _component_router_link = Object(vue_esm_browser["P" /* resolveComponent */])("router-link");

  return Object(vue_esm_browser["G" /* openBlock */])(), Object(vue_esm_browser["j" /* createBlock */])("div", null, [Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_1, [Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_2, [Object(vue_esm_browser["o" /* createVNode */])("h1", null, Object(vue_esm_browser["T" /* toDisplayString */])($options.langConfig[1]) + "lll", 1), Object(vue_esm_browser["o" /* createVNode */])("p", null, Object(vue_esm_browser["T" /* toDisplayString */])($options.langConfig[2]), 1)])]), Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_3, [_hoisted_4, Object(vue_esm_browser["o" /* createVNode */])("div", {
    class: "jumbotron-red",
    style: {
      height: $data.mainImgOffset + 'px'
    }
  }, [_hoisted_5], 4)], 512), Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_6, [Object(vue_esm_browser["o" /* createVNode */])("a", _hoisted_7, [_hoisted_8, Object(vue_esm_browser["o" /* createVNode */])("div", null, [Object(vue_esm_browser["o" /* createVNode */])("p", null, [Object(vue_esm_browser["n" /* createTextVNode */])(Object(vue_esm_browser["T" /* toDisplayString */])($options.sponsorLabel) + " ", 1), _hoisted_9]), _hoisted_10])]), Object(vue_esm_browser["o" /* createVNode */])("a", _hoisted_11, [_hoisted_12, Object(vue_esm_browser["o" /* createVNode */])("div", null, [Object(vue_esm_browser["o" /* createVNode */])("p", null, [Object(vue_esm_browser["n" /* createTextVNode */])(Object(vue_esm_browser["T" /* toDisplayString */])($options.sponsorLabel) + " ", 1), _hoisted_13]), _hoisted_14])])]), Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_15, [Object(vue_esm_browser["o" /* createVNode */])("ul", _hoisted_16, [Object(vue_esm_browser["o" /* createVNode */])("li", null, [Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_17, [_hoisted_18, Object(vue_esm_browser["o" /* createVNode */])("h3", null, Object(vue_esm_browser["T" /* toDisplayString */])($options.langConfig[3]), 1), Object(vue_esm_browser["o" /* createVNode */])("p", null, Object(vue_esm_browser["T" /* toDisplayString */])($options.langConfig[4]), 1), Object(vue_esm_browser["o" /* createVNode */])(_component_router_link, {
    "active-class": "active",
    to: "/" + $data.lang + "/guide/design",
    exact: ""
  }, {
    default: _withId(function () {
      return [Object(vue_esm_browser["n" /* createTextVNode */])(Object(vue_esm_browser["T" /* toDisplayString */])($options.langConfig[5]), 1)];
    }),
    _: 1
  }, 8, ["to"])])]), Object(vue_esm_browser["o" /* createVNode */])("li", null, [Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_19, [_hoisted_20, Object(vue_esm_browser["o" /* createVNode */])("h3", null, Object(vue_esm_browser["T" /* toDisplayString */])($options.langConfig[6]), 1), Object(vue_esm_browser["o" /* createVNode */])("p", null, Object(vue_esm_browser["T" /* toDisplayString */])($options.langConfig[7]), 1), Object(vue_esm_browser["o" /* createVNode */])(_component_router_link, {
    "active-class": "active",
    to: "/" + $data.lang + "/component/layout",
    exact: ""
  }, {
    default: _withId(function () {
      return [Object(vue_esm_browser["n" /* createTextVNode */])(Object(vue_esm_browser["T" /* toDisplayString */])($options.langConfig[5]), 1)];
    }),
    _: 1
  }, 8, ["to"])])]), Object(vue_esm_browser["o" /* createVNode */])("li", null, [Object(vue_esm_browser["o" /* createVNode */])("div", _hoisted_21, [_hoisted_22, Object(vue_esm_browser["o" /* createVNode */])("h3", null, Object(vue_esm_browser["T" /* toDisplayString */])($options.langConfig[8]), 1), Object(vue_esm_browser["o" /* createVNode */])("p", null, Object(vue_esm_browser["T" /* toDisplayString */])($options.langConfig[9]), 1), Object(vue_esm_browser["o" /* createVNode */])(_component_router_link, {
    "active-class": "active",
    to: "/" + $data.lang + "/resource",
    exact: ""
  }, {
    default: _withId(function () {
      return [Object(vue_esm_browser["n" /* createTextVNode */])(Object(vue_esm_browser["T" /* toDisplayString */])($options.langConfig[5]), 1)];
    }),
    _: 1
  }, 8, ["to"])])])])])]);
});
// CONCATENATED MODULE: ./website/pages/index.vue?vue&type=template&id=2b854c51&scoped=true

// EXTERNAL MODULE: ./website/i18n/page.json
var page = __webpack_require__(478);

// EXTERNAL MODULE: ./node_modules/throttle-debounce/index.umd.js
var index_umd = __webpack_require__(479);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--11-0!./website/pages/index.vue?vue&type=script&lang=js


/* harmony default export */ var pagesvue_type_script_lang_js = ({
  data: function data() {
    return {
      lang: this.$route.meta.lang,
      mainImgOffset: 0
    };
  },
  computed: {
    sponsorLabel: function sponsorLabel() {
      return this.lang === 'zh-CN' ? '赞助商' : 'Sponsored by';
    },
    langConfig: function langConfig() {
      var _this = this;

      return page.filter(function (config) {
        return config.lang === _this.lang;
      })[0].pages.index;
    }
  },
  created: function created() {
    var _this2 = this;

    this.throttledHandleScroll = Object(index_umd["throttle"])(10, true, function (index) {
      _this2.handleScroll(index);
    });
  },
  beforeUnmount: function beforeUnmount() {
    window.removeEventListener('scroll', this.throttledHandleScroll);
  },
  mounted: function mounted() {
    window.addEventListener('scroll', this.throttledHandleScroll);
  },
  methods: {
    handleScroll: function handleScroll() {
      var ele = this.$refs.indexMainImg;
      var rect = ele.getBoundingClientRect();
      var eleHeight = ele.clientHeight + 55;
      var calHeight = (180 - rect.top) * 2;
      if (calHeight < 0) calHeight = 0;
      if (calHeight > eleHeight) calHeight = eleHeight;
      this.mainImgOffset = calHeight;
    }
  }
});
// CONCATENATED MODULE: ./website/pages/index.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./website/pages/index.vue?vue&type=style&index=0&id=2b854c51&lang=scss&scoped=true
var pagesvue_type_style_index_0_id_2b854c51_lang_scss_scoped_true = __webpack_require__(491);

// CONCATENATED MODULE: ./website/pages/index.vue





pagesvue_type_script_lang_js.render = render
pagesvue_type_script_lang_js.__scopeId = "data-v-2b854c51"

/* harmony default export */ var pages = __webpack_exports__["default"] = (pagesvue_type_script_lang_js);

/***/ }),

/***/ 478:
/***/ (function(module) {

module.exports = JSON.parse("[{\"lang\":\"zh-CN\",\"pages\":{\"index\":{\"1\":\"网站快速成型工具\",\"2\":\"Hongluan ui，一套为开发者、设计师和产品经理准备的基于 Vue 3.0 的桌面端组件库\",\"3\":\"指南\",\"4\":\"了解设计指南，帮助产品设计人员搭建逻辑清晰、结构合理且高效易用的产品。\",\"5\":\"查看详情\",\"6\":\"组件\",\"7\":\"使用组件 Demo 快速体验交互细节；使用前端框架封装的代码帮助工程师快速开发。\",\"8\":\"资源\",\"9\":\"下载相关资源，用其快速搭建页面原型或高保真视觉稿，提升产品设计效率。\",\"10\":\"主题\",\"11\":\"在线主题编辑器，可视化定制和管理站点主题、组件样式\",\"12\":\"主题定制功能上线\",\"13\":\"点击开始编辑\",\"14\":\"尝试您的新主题\",\"lang\":\"zh-CN\",\"titleSize\":\"34\",\"paraSize\":\"18\"},\"component\":{},\"theme\":{\"1\":\"官方主题\",\"2\":\"我的主题\",\"3\":\"主题名称\"},\"theme-preview\":{\"1\":\"返回\"},\"theme-nav\":{},\"changelog\":{\"1\":\"更新日志\",\"2\":\"zh-CN\"},\"design\":{\"1\":\"设计原则\",\"2\":\"一致\",\"3\":\"Consistency\",\"4\":\"反馈\",\"5\":\"Feedback\",\"6\":\"效率\",\"7\":\"Efficiency\",\"8\":\"可控\",\"9\":\"Controllability\",\"10\":\"一致性 Consistency\",\"11\":\"与现实生活一致：\",\"12\":\"与现实生活的流程、逻辑保持一致，遵循用户习惯的语言和概念；\",\"13\":\"在界面中一致：\",\"14\":\"所有的元素和结构需保持一致，比如：设计样式、图标和文本、元素的位置等。\",\"15\":\"反馈 Feedback\",\"16\":\"控制反馈：\",\"17\":\"通过界面样式和交互动效让用户可以清晰的感知自己的操作；\",\"18\":\"页面反馈：\",\"19\":\"操作后，通过页面元素的变化清晰地展现当前状态。\",\"20\":\"效率 Efficiency\",\"21\":\"简化流程：\",\"22\":\"设计简洁直观的操作流程；\",\"23\":\"清晰明确：\",\"24\":\"语言表达清晰且表意明确，让用户快速理解进而作出决策；\",\"25\":\"帮助用户识别：\",\"26\":\"界面简单直白，让用户快速识别而非回忆，减少用户记忆负担。\",\"27\":\"可控 Controllability\",\"28\":\"用户决策：\",\"29\":\"根据场景可给予用户操作建议或安全提示，但不能代替用户进行决策；\",\"30\":\"结果可控：\",\"31\":\"用户可以自由的进行操作，包括撤销、回退和终止当前操作等。\"},\"guide\":{\"1\":\"设计原则\",\"2\":\"导航\"},\"nav\":{\"1\":\"导航\",\"2\":\"导航可以解决用户在访问页面时：在哪里，去哪里，怎样去的问题。一般导航会有「侧栏导航」和「顶部导航」2 种类型。\",\"3\":\"选择合适的导航\",\"4\":\"选择合适的导航可以让用户在产品的使用过程中非常流畅，相反若是不合适就会引起用户操作不适（方向不明确），以下是「侧栏导航」和 「顶部导航」的区别。\",\"5\":\"侧栏导航\",\"6\":\"可将导航栏固定在左侧，提高导航可见性，方便页面之间切换；顶部可放置常用工具，如搜索条、帮助按钮、通知按钮等。适用于中后台的管理型、工具型网站。\",\"7\":\"一级类目\",\"8\":\"适用于结构简单的网站：只有一级页面时，不需要使用面包屑。\",\"9\":\"二级类目\",\"10\":\"侧栏中最多可显示两级导航；当使用二级导航时，我们建议搭配使用面包屑，方便用户定位自己的位置和快速返回。\",\"11\":\"三级类目\",\"12\":\"适用于较复杂的工具型后台，左侧栏为一级导航，中间栏可显示其对应的二级导航，也可放置其他的工具型选项。\",\"13\":\"顶部导航\",\"14\":\"顺应了从上至下的正常浏览顺序，方便浏览信息；顶部宽度限制了导航的数量和文本长度。\",\"15\":\"适用于导航较少，页面篇幅较长的网站。\"},\"resource\":{\"1\":\"资源\",\"2\":\"整理中\",\"3\":\"Axure Components\",\"4\":\"通过在 Axure 中导入 Element 组件库，可以在交互设计阶段方便地调用常用的组件\",\"5\":\"下载\",\"6\":\"Sketch Template\",\"7\":\"从 Element Template 快速调用常用组件，在提升设计效率的同时，保证统一的视觉风格\",\"8\":\"组件交互文档\",\"9\":\"进一步查看 Element 交互文档，从一个较为微观的角度详细地了解各个组件的交互细节\",\"paraHeight\":\"1.8\",\"placeholder1\":\"整理中\",\"placeholder2\":\"设计资源正在整理和完善中\"}}}]");

/***/ }),

/***/ 479:
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? factory(exports) :
	undefined;
}(this, (function (exports) { 'use strict';

	/* eslint-disable no-undefined,no-param-reassign,no-shadow */

	/**
	 * Throttle execution of a function. Especially useful for rate limiting
	 * execution of handlers on events like resize and scroll.
	 *
	 * @param  {number}    delay -          A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250 (or even higher) are most useful.
	 * @param  {boolean}   [noTrailing] -   Optional, defaults to false. If noTrailing is true, callback will only execute every `delay` milliseconds while the
	 *                                    throttled-function is being called. If noTrailing is false or unspecified, callback will be executed one final time
	 *                                    after the last throttled-function call. (After the throttled-function has not been called for `delay` milliseconds,
	 *                                    the internal counter is reset).
	 * @param  {Function}  callback -       A function to be executed after delay milliseconds. The `this` context and all arguments are passed through, as-is,
	 *                                    to `callback` when the throttled-function is executed.
	 * @param  {boolean}   [debounceMode] - If `debounceMode` is true (at begin), schedule `clear` to execute after `delay` ms. If `debounceMode` is false (at end),
	 *                                    schedule `callback` to execute after `delay` ms.
	 *
	 * @returns {Function}  A new, throttled, function.
	 */
	function throttle (delay, noTrailing, callback, debounceMode) {
	  /*
	   * After wrapper has stopped being called, this timeout ensures that
	   * `callback` is executed at the proper times in `throttle` and `end`
	   * debounce modes.
	   */
	  var timeoutID;
	  var cancelled = false; // Keep track of the last time `callback` was executed.

	  var lastExec = 0; // Function to clear existing timeout

	  function clearExistingTimeout() {
	    if (timeoutID) {
	      clearTimeout(timeoutID);
	    }
	  } // Function to cancel next exec


	  function cancel() {
	    clearExistingTimeout();
	    cancelled = true;
	  } // `noTrailing` defaults to falsy.


	  if (typeof noTrailing !== 'boolean') {
	    debounceMode = callback;
	    callback = noTrailing;
	    noTrailing = undefined;
	  }
	  /*
	   * The `wrapper` function encapsulates all of the throttling / debouncing
	   * functionality and when executed will limit the rate at which `callback`
	   * is executed.
	   */


	  function wrapper() {
	    for (var _len = arguments.length, arguments_ = new Array(_len), _key = 0; _key < _len; _key++) {
	      arguments_[_key] = arguments[_key];
	    }

	    var self = this;
	    var elapsed = Date.now() - lastExec;

	    if (cancelled) {
	      return;
	    } // Execute `callback` and update the `lastExec` timestamp.


	    function exec() {
	      lastExec = Date.now();
	      callback.apply(self, arguments_);
	    }
	    /*
	     * If `debounceMode` is true (at begin) this is used to clear the flag
	     * to allow future `callback` executions.
	     */


	    function clear() {
	      timeoutID = undefined;
	    }

	    if (debounceMode && !timeoutID) {
	      /*
	       * Since `wrapper` is being called for the first time and
	       * `debounceMode` is true (at begin), execute `callback`.
	       */
	      exec();
	    }

	    clearExistingTimeout();

	    if (debounceMode === undefined && elapsed > delay) {
	      /*
	       * In throttle mode, if `delay` time has been exceeded, execute
	       * `callback`.
	       */
	      exec();
	    } else if (noTrailing !== true) {
	      /*
	       * In trailing throttle mode, since `delay` time has not been
	       * exceeded, schedule `callback` to execute `delay` ms after most
	       * recent execution.
	       *
	       * If `debounceMode` is true (at begin), schedule `clear` to execute
	       * after `delay` ms.
	       *
	       * If `debounceMode` is false (at end), schedule `callback` to
	       * execute after `delay` ms.
	       */
	      timeoutID = setTimeout(debounceMode ? clear : exec, debounceMode === undefined ? delay - elapsed : delay);
	    }
	  }

	  wrapper.cancel = cancel; // Return the wrapper function.

	  return wrapper;
	}

	/* eslint-disable no-undefined */
	/**
	 * Debounce execution of a function. Debouncing, unlike throttling,
	 * guarantees that a function is only executed a single time, either at the
	 * very beginning of a series of calls, or at the very end.
	 *
	 * @param  {number}   delay -         A zero-or-greater delay in milliseconds. For event callbacks, values around 100 or 250 (or even higher) are most useful.
	 * @param  {boolean}  [atBegin] -     Optional, defaults to false. If atBegin is false or unspecified, callback will only be executed `delay` milliseconds
	 *                                  after the last debounced-function call. If atBegin is true, callback will be executed only at the first debounced-function call.
	 *                                  (After the throttled-function has not been called for `delay` milliseconds, the internal counter is reset).
	 * @param  {Function} callback -      A function to be executed after delay milliseconds. The `this` context and all arguments are passed through, as-is,
	 *                                  to `callback` when the debounced-function is executed.
	 *
	 * @returns {Function} A new, debounced function.
	 */

	function debounce (delay, atBegin, callback) {
	  return callback === undefined ? throttle(delay, atBegin, false) : throttle(delay, callback, atBegin !== false);
	}

	exports.debounce = debounce;
	exports.throttle = throttle;

	Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=index.umd.js.map


/***/ }),

/***/ 483:
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(12);
            var content = __webpack_require__(492);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ 491:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_4_3_node_modules_vue_loader_dist_index_js_ref_11_0_index_vue_vue_type_style_index_0_id_2b854c51_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(483);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_4_3_node_modules_vue_loader_dist_index_js_ref_11_0_index_vue_vue_type_style_index_0_id_2b854c51_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_cjs_js_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_dist_cjs_js_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_sass_loader_dist_cjs_js_ref_4_3_node_modules_vue_loader_dist_index_js_ref_11_0_index_vue_vue_type_style_index_0_id_2b854c51_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ 492:
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

}]);