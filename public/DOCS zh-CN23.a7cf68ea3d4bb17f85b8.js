(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[16],{

/***/ 472:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/installation.md?vue&type=template&id=3aaf195f

var _hoisted_1 = {
  class: "doc-main-content"
};

var _hoisted_2 = /*#__PURE__*/Object(vue_esm_browser["m" /* createStaticVNode */])("<div class=\"doc-content\"><h2 id=\"an-zhuang\"><a class=\"header-anchor\" href=\"#an-zhuang\"></a> 安装</h2><h3 id=\"npm-an-zhuang\"><a class=\"header-anchor\" href=\"#npm-an-zhuang\"></a> npm 安装</h3><p>推荐使用 npm 的方式安装，它能更好地和 <a href=\"https://webpack.js.org/\">webpack</a> 打包工具配合使用。</p><pre><code class=\"hljs language-shell\">npm install hongluan-ui --save\n</code></pre><p>如果是通过 npm 安装，并希望配合 webpack 使用，请阅读下一节：<a href=\"/#/zh-CN/component/quickstart\">快速上手</a>。</p></div>", 1);

function render(_ctx, _cache) {
  var _component_right_nav = Object(vue_esm_browser["P" /* resolveComponent */])("right-nav");

  return Object(vue_esm_browser["G" /* openBlock */])(), Object(vue_esm_browser["j" /* createBlock */])("section", _hoisted_1, [_hoisted_2, Object(vue_esm_browser["o" /* createVNode */])(_component_right_nav)]);
}
// CONCATENATED MODULE: ./website/docs/zh-CN/installation.md?vue&type=template&id=3aaf195f

// CONCATENATED MODULE: ./website/docs/zh-CN/installation.md

const script = {}
script.render = render

/* harmony default export */ var installation = __webpack_exports__["default"] = (script);

/***/ })

}]);