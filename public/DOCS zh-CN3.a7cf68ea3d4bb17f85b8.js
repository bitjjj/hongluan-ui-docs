(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[23],{

/***/ 428:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/badge.md?vue&type=template&id=27d0fbcd

var badgevue_type_template_id_27d0fbcd_hoisted_1 = {
  class: "doc-main-content"
};
var badgevue_type_template_id_27d0fbcd_hoisted_2 = {
  class: "doc-content"
};

var badgevue_type_template_id_27d0fbcd_hoisted_3 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h1", null, "Badge 徽章", -1);

var badgevue_type_template_id_27d0fbcd_hoisted_4 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "徽章（badge）是一种小型的用于计数和打标签的组件。", -1);

var _hoisted_5 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("定义"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "value"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("属性，它接受"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "Number"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("或者"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "String"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("。")])], -1);

var _hoisted_6 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-row gap=\"xl\" justify=\"center\">\n  <hl-col span=\"col-lg-12\">\n    <p>Number</p>\n    <hl-badge :value=\"1\" type=\"primary\" class=\"m-r-md\"></hl-badge>\n    <hl-badge :value=\"2\" type=\"primary\" outline class=\"m-r-md\"></hl-badge>\n    <hl-badge :value=\"3\" type=\"primary\" round class=\"m-r-md\"></hl-badge>\n  </hl-col>\n  <hl-col span=\"col-lg-12\">\n    <p>String</p>\n    <hl-badge value=\"Badge\" type=\"primary\" class=\"m-r-md\"></hl-badge>\n    <hl-badge\n      value=\"Badge Outline\"\n      type=\"primary\"\n      outline\n      class=\"m-r-md\"\n    ></hl-badge>\n    <hl-badge\n      value=\"Badge Round\"\n      type=\"primary\"\n      round\n      class=\"m-r-md\"\n    ></hl-badge>\n  </hl-col>\n</hl-row>\n")], -1);

var _hoisted_7 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "hui-zhang-yan-se"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#hui-zhang-yan-se"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 徽章颜色")], -1);

var _hoisted_8 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "badge"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 组件的颜色与状态颜色保持一致。")], -1);

var _hoisted_9 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-badge value=\"primary\" type=\"primary\" class=\"m-r-md\"></hl-badge>\n<hl-badge value=\"success\" type=\"success\" class=\"m-r-md\"></hl-badge>\n<hl-badge value=\"danger\" type=\"danger\" class=\"m-r-md\"></hl-badge>\n<hl-badge value=\"warning\" type=\"warning\" class=\"m-r-md\"></hl-badge>\n<hl-badge value=\"info\" type=\"info\" class=\"m-r-md\"></hl-badge>\n")], -1);

var _hoisted_10 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "xuan-fu-wei-zhi"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#xuan-fu-wei-zhi"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 悬浮位置")], -1);

var _hoisted_11 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("设置 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "position"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("属性可以使 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "Badge"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 悬浮在父元素的四个角上。")], -1);

var _hoisted_12 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-button class=\"m-r-md\">左上悬浮<hl-badge value=\"LT\" type=\"danger\" position=\"lt\" round></hl-badge></hl-button>\n<hl-button class=\"m-r-md\">左下悬浮<hl-badge value=\"LB\" type=\"danger\" position=\"lb\" round></hl-badge></hl-button>\n<hl-button class=\"m-r-md\">右上悬浮<hl-badge value=\"RT\" type=\"danger\" position=\"rt\" round></hl-badge></hl-button>\n<hl-button class=\"m-r-md\">右下悬浮<hl-badge value=\"RB\" type=\"danger\" position=\"rb\" round></hl-badge></hl-button>\n")], -1);

var _hoisted_13 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "zui-da-zhi"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#zui-da-zhi"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 最大值")], -1);

var _hoisted_14 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "max"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("属性可自定义最大值，需要注意的是，只有当"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "value"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("为"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "Number"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("时，它才会生效。")], -1);

var _hoisted_15 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-button class=\"m-r-md\"\n  >评论<hl-badge\n    type=\"danger\"\n    position=\"rt\"\n    :value=\"200\"\n    :max=\"99\"\n    round\n  ></hl-badge\n></hl-button>\n<hl-button class=\"m-r-md\"\n  >回复<hl-badge\n    type=\"danger\"\n    position=\"rt\"\n    :value=\"100\"\n    :max=\"10\"\n    round\n  ></hl-badge\n></hl-button>\n\n<style>\n  .item {\n    margin-top: 10px;\n    margin-right: 40px;\n  }\n</style>\n")], -1);

var _hoisted_16 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "yuan-dian-hui-zhang"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#yuan-dian-hui-zhang"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 圆点徽章")], -1);

var _hoisted_17 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("设置"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "dot"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("属性可使徽章组件变成一个 12 x 12 像素的圆点。")], -1);

var _hoisted_18 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-button class=\"m-r-md\">\n  小红点<hl-badge dot type=\"danger\" position=\"rt\">数据查询</hl-badge>\n</hl-button>\n\n<hl-button type=\"primary\" class=\"m-r-md\">\n  <hl-badge dot class=\"m-r-sm\"></hl-badge>数据查询\n</hl-button>\n\n<style>\n  .item {\n    margin-top: 10px;\n    margin-right: 40px;\n  }\n</style>\n")], -1);

var _hoisted_19 = /*#__PURE__*/Object(vue_esm_browser["m" /* createStaticVNode */])("<h2 id=\"attributes\"><a class=\"header-anchor\" href=\"#attributes\"></a> Attributes</h2><table><thead><tr><th>参数</th><th>说明</th><th>类型</th><th>可选值</th><th>默认值</th></tr></thead><tbody><tr><td>value</td><td>显示值</td><td>string / number</td><td>—</td><td>—</td></tr><tr><td>max</td><td>最大值，超过最大值会显示 &#39;{max}+&#39;，要求 value 是 Number 类型</td><td>number</td><td>—</td><td>99</td></tr><tr><td>dot</td><td>小圆点</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>round</td><td>是否是圆形</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>hidden</td><td>隐藏 badge</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>type</td><td>类型</td><td>string</td><td>primary / success / warning / danger / info</td><td>—</td></tr><tr><td>position</td><td>位置</td><td>string</td><td>lt / rt / lb / rb</td><td>—</td></tr></tbody></table>", 2);

function badgevue_type_template_id_27d0fbcd_render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_hl_demo0 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo0");

  var _component_demo_block = Object(vue_esm_browser["P" /* resolveComponent */])("demo-block");

  var _component_hl_demo1 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo1");

  var _component_hl_demo2 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo2");

  var _component_hl_demo3 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo3");

  var _component_hl_demo4 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo4");

  var _component_right_nav = Object(vue_esm_browser["P" /* resolveComponent */])("right-nav");

  return Object(vue_esm_browser["G" /* openBlock */])(), Object(vue_esm_browser["j" /* createBlock */])("section", badgevue_type_template_id_27d0fbcd_hoisted_1, [Object(vue_esm_browser["o" /* createVNode */])("div", badgevue_type_template_id_27d0fbcd_hoisted_2, [badgevue_type_template_id_27d0fbcd_hoisted_3, badgevue_type_template_id_27d0fbcd_hoisted_4, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo0)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_6];
    }),
    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_5];
    }),
    _: 1
  }), _hoisted_7, _hoisted_8, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo1)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_9];
    }),
    _: 1
  }), _hoisted_10, _hoisted_11, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo2)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_12];
    }),
    _: 1
  }), _hoisted_13, _hoisted_14, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo3)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_15];
    }),
    _: 1
  }), _hoisted_16, _hoisted_17, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo4)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_18];
    }),
    _: 1
  }), _hoisted_19]), Object(vue_esm_browser["o" /* createVNode */])(_component_right_nav)]);
}
// CONCATENATED MODULE: ./website/docs/zh-CN/badge.md?vue&type=template&id=27d0fbcd

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/extends.js
var helpers_extends = __webpack_require__(4);
var extends_default = /*#__PURE__*/__webpack_require__.n(helpers_extends);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/badge.md?vue&type=script&lang=ts


/* harmony default export */ var badgevue_type_script_lang_ts = ({
  name: 'component-doc',
  components: {
    "hl-demo0": function () {
      var _createVNode = vue_esm_browser["o" /* createVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createVNode("p", null, "Number", -1);

      var _hoisted_2 = /*#__PURE__*/_createVNode("p", null, "String", -1);

      function render(_ctx, _cache) {
        var _component_hl_badge = _resolveComponent("hl-badge");

        var _component_hl_col = _resolveComponent("hl-col");

        var _component_hl_row = _resolveComponent("hl-row");

        return _openBlock(), _createBlock(_component_hl_row, {
          gap: "xl",
          justify: "center"
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_col, {
              span: "col-lg-12"
            }, {
              default: _withCtx(function () {
                return [_hoisted_1, _createVNode(_component_hl_badge, {
                  value: 1,
                  type: "primary",
                  class: "m-r-md"
                }), _createVNode(_component_hl_badge, {
                  value: 2,
                  type: "primary",
                  outline: "",
                  class: "m-r-md"
                }), _createVNode(_component_hl_badge, {
                  value: 3,
                  type: "primary",
                  round: "",
                  class: "m-r-md"
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: "col-lg-12"
            }, {
              default: _withCtx(function () {
                return [_hoisted_2, _createVNode(_component_hl_badge, {
                  value: "Badge",
                  type: "primary",
                  class: "m-r-md"
                }), _createVNode(_component_hl_badge, {
                  value: "Badge Outline",
                  type: "primary",
                  outline: "",
                  class: "m-r-md"
                }), _createVNode(_component_hl_badge, {
                  value: "Badge Round",
                  type: "primary",
                  round: "",
                  class: "m-r-md"
                })];
              }),
              _: 1
            })];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo1": function () {
      var _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      function render(_ctx, _cache) {
        var _component_hl_badge = _resolveComponent("hl-badge");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_badge, {
          value: "primary",
          type: "primary",
          class: "m-r-md"
        }), _createVNode(_component_hl_badge, {
          value: "success",
          type: "success",
          class: "m-r-md"
        }), _createVNode(_component_hl_badge, {
          value: "danger",
          type: "danger",
          class: "m-r-md"
        }), _createVNode(_component_hl_badge, {
          value: "warning",
          type: "warning",
          class: "m-r-md"
        }), _createVNode(_component_hl_badge, {
          value: "info",
          type: "info",
          class: "m-r-md"
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo2": function () {
      var _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("左上悬浮");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("左下悬浮");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("右上悬浮");

      var _hoisted_4 = /*#__PURE__*/_createTextVNode("右下悬浮");

      function render(_ctx, _cache) {
        var _component_hl_badge = _resolveComponent("hl-badge");

        var _component_hl_button = _resolveComponent("hl-button");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_button, {
          class: "m-r-md"
        }, {
          default: _withCtx(function () {
            return [_hoisted_1, _createVNode(_component_hl_badge, {
              value: "LT",
              type: "danger",
              position: "lt",
              round: ""
            })];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          class: "m-r-md"
        }, {
          default: _withCtx(function () {
            return [_hoisted_2, _createVNode(_component_hl_badge, {
              value: "LB",
              type: "danger",
              position: "lb",
              round: ""
            })];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          class: "m-r-md"
        }, {
          default: _withCtx(function () {
            return [_hoisted_3, _createVNode(_component_hl_badge, {
              value: "RT",
              type: "danger",
              position: "rt",
              round: ""
            })];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          class: "m-r-md"
        }, {
          default: _withCtx(function () {
            return [_hoisted_4, _createVNode(_component_hl_badge, {
              value: "RB",
              type: "danger",
              position: "rb",
              round: ""
            })];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo3": function () {
      var _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode("评论");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("回复");

      function render(_ctx, _cache) {
        var _component_hl_badge = _resolveComponent("hl-badge");

        var _component_hl_button = _resolveComponent("hl-button");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_button, {
          class: "m-r-md"
        }, {
          default: _withCtx(function () {
            return [_hoisted_1, _createVNode(_component_hl_badge, {
              type: "danger",
              position: "rt",
              value: 200,
              max: 99,
              round: ""
            })];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          class: "m-r-md"
        }, {
          default: _withCtx(function () {
            return [_hoisted_2, _createVNode(_component_hl_badge, {
              type: "danger",
              position: "rt",
              value: 100,
              max: 10,
              round: ""
            })];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo4": function () {
      var _createTextVNode = vue_esm_browser["n" /* createTextVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _Fragment = vue_esm_browser["b" /* Fragment */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createTextVNode(" 小红点");

      var _hoisted_2 = /*#__PURE__*/_createTextVNode("数据查询");

      var _hoisted_3 = /*#__PURE__*/_createTextVNode("数据查询 ");

      function render(_ctx, _cache) {
        var _component_hl_badge = _resolveComponent("hl-badge");

        var _component_hl_button = _resolveComponent("hl-button");

        return _openBlock(), _createBlock(_Fragment, null, [_createVNode(_component_hl_button, {
          class: "m-r-md"
        }, {
          default: _withCtx(function () {
            return [_hoisted_1, _createVNode(_component_hl_badge, {
              dot: "",
              type: "danger",
              position: "rt"
            }, {
              default: _withCtx(function () {
                return [_hoisted_2];
              }),
              _: 1
            })];
          }),
          _: 1
        }), _createVNode(_component_hl_button, {
          type: "primary",
          class: "m-r-md"
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_badge, {
              dot: "",
              class: "m-r-sm"
            }), _hoisted_3];
          }),
          _: 1
        })], 64);
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }()
  }
});
// CONCATENATED MODULE: ./website/docs/zh-CN/badge.md?vue&type=script&lang=ts
 
// CONCATENATED MODULE: ./website/docs/zh-CN/badge.md



badgevue_type_script_lang_ts.render = badgevue_type_template_id_27d0fbcd_render

/* harmony default export */ var badge = __webpack_exports__["default"] = (badgevue_type_script_lang_ts);

/***/ })

}]);