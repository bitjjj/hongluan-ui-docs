(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[11],{

/***/ 442:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-browser.js
var vue_esm_browser = __webpack_require__(0);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist/templateLoader.js??ref--6!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/icon.md?vue&type=template&id=5810ea12

var iconvue_type_template_id_5810ea12_hoisted_1 = {
  class: "doc-main-content"
};
var iconvue_type_template_id_5810ea12_hoisted_2 = {
  class: "doc-content"
};

var _hoisted_3 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h1", null, "Icon 图标", -1);

var _hoisted_4 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("通过 icon 组件来调用 svg 图标。例如 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "<hl-icon name=\"Home\" />"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("。"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "name"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("第一个字母大小写都可以，推荐大写")], -1);

var _hoisted_5 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-icon name=\"Home\" size=\"lg\"></hl-icon>\n")], -1);

var _hoisted_6 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "chi-cun-she-zhi"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#chi-cun-she-zhi"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 尺寸设置")], -1);

var _hoisted_7 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("你可以定义 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "icon"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 组件的尺寸，例如："), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "size=\"lg\""), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("，也可以自定义宽高。")], -1);

var _hoisted_8 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-group indent=\"var(--md)\">\n  <hl-icon name=\"Delete\" size=\"xxs\"></hl-icon>\n  <hl-icon name=\"Delete\" size=\"xs\"></hl-icon>\n  <hl-icon name=\"Delete\" size=\"sm\"></hl-icon>\n  <hl-icon name=\"Delete\" size=\"md\"></hl-icon>\n  <hl-icon name=\"Delete\" size=\"lg\"></hl-icon>\n  <hl-icon name=\"Delete\" size=\"xl\"></hl-icon>\n  <hl-icon name=\"Delete\" size=\"xxl\"></hl-icon>\n  <hl-icon name=\"Delete\" width=\"56px\" height=\"56px\"></hl-icon>\n</hl-group>\n")], -1);

var _hoisted_9 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "yan-se-she-zhi"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#yan-se-she-zhi"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 颜色设置")], -1);

var _hoisted_10 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "icon"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 组件支持状态颜色，"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "class=\"primary\""), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("。 你也可以自定义颜色，例如："), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "color=\"#ff6600\""), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("，")], -1);

var _hoisted_11 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-group full indent=\"var(--md)\">\n  <hl-icon name=\"CameraOff\" type=\"primary\" size=\"lg\"></hl-icon>\n  <hl-icon name=\"Layout\" type=\"danger\" size=\"lg\"></hl-icon>\n  <hl-icon name=\"UserGroup\" type=\"warning\" size=\"lg\"></hl-icon>\n  <hl-icon name=\"Server\" type=\"success\" size=\"lg\"></hl-icon>\n  <hl-icon name=\"News\" type=\"info\" size=\"lg\"></hl-icon>\n  <hl-icon name=\"Setting\" type=\"danger\" size=\"lg\"></hl-icon>\n  <hl-icon name=\"Picture\" type=\"success\" size=\"lg\"></hl-icon>\n  <hl-icon name=\"Talk\" size=\"lg\" color=\"#888\"></hl-icon>\n</hl-group>\n\n\n")], -1);

var _hoisted_12 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "jian-bian-she-zhi"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#jian-bian-she-zhi"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 渐变设置")], -1);

var _hoisted_13 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "gradient"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 属性可使 Icon 呈现为渐变样式，它还可以为状态色渲染一套漂亮的渐变样式。")], -1);

var _hoisted_14 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "start-color"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("属性可以自定义渐变开始的颜色。")])], -1);

var _hoisted_15 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-group full indent=\"var(--md)\">\n  <hl-icon name=\"CameraOff\" type=\"primary\" size=\"lg\" border=\"1.2\" gradient></hl-icon>\n  <hl-icon name=\"Layout\" type=\"danger\" size=\"lg\" border=\"1.2\" gradient></hl-icon>\n  <hl-icon name=\"UserGroup\" type=\"warning\" size=\"lg\" border=\"1.2\" gradient></hl-icon>\n  <hl-icon name=\"Server\" type=\"success\" size=\"lg\" border=\"1.2\" gradient></hl-icon>\n  <hl-icon name=\"News\" type=\"info\" size=\"lg\" border=\"1.2\" gradient></hl-icon>\n  <hl-icon name=\"Setting\" type=\"danger\" size=\"lg\" border=\"1.2\" gradient></hl-icon>\n  <hl-icon name=\"Picture\" type=\"success\" size=\"lg\" border=\"1.2\" gradient></hl-icon>\n  <hl-icon name=\"Talk\" size=\"lg\" border=\"1.2\" start-color=\"#fff\" color=\"#888\" gradient></hl-icon>\n</hl-group>\n")], -1);

var _hoisted_16 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "tou-ming-du-yu-xian-tiao"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#tou-ming-du-yu-xian-tiao"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 透明度与线条")], -1);

var _hoisted_17 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])("通过"), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "opacity"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 和 "), /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "border"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 属性来设置图标的透明度和线条粗细。")], -1);

var _hoisted_18 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("div", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "Svg 图形的特性，border 线条会根据图片的比例而缩放。")], -1);

var _hoisted_19 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-row>\n  <hl-col span=\"col\">\n    <p>填充透明度</p>\n    <hl-group indent=\"var(--lg)\">\n      <hl-icon name=\"Delete\" :opacity=\"0\"></hl-icon>\n      <hl-icon name=\"Delete\" :opacity=\".2\"></hl-icon>\n      <hl-icon name=\"Delete\" :opacity=\".5\"></hl-icon>\n    </hl-group>\n  </hl-col>\n  <hl-col span=\"col\">\n    <p>线条粗细</p>\n    <hl-group indent=\"var(--lg)\">\n      <hl-icon name=\"Delete\" :border=\"1\"></hl-icon>\n      <hl-icon name=\"Delete\" :border=\"1.5\"></hl-icon>\n      <hl-icon name=\"Delete\" :border=\"2\"></hl-icon>\n    </hl-group>\n  </hl-col>\n</hl-row>\n")], -1);

var _hoisted_20 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "bei-jing-tian-chong"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#bei-jing-tian-chong"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 背景填充")], -1);

var _hoisted_21 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", null, "fill"), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 属性可为 icon 加上一个背景色")], -1);

var _hoisted_22 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("pre", null, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("code", {
  class: "html"
}, "<hl-group indent=\"var(--lg)\" style=\"align-items: center\">\n  <hl-icon name=\"Airplay\" type=\"primary\" size=\"xxs\" fill></hl-icon>\n  <hl-icon name=\"Picture\" type=\"danger\" size=\"xs\" fill></hl-icon>\n  <hl-icon name=\"Briefcase\" type=\"warning\" size=\"md\" fill></hl-icon>\n  <hl-icon name=\"FolderIn\" type=\"success\" size=\"lg\" fill></hl-icon>\n  <hl-icon name=\"Application\" type=\"info\" size=\"xl\" fill></hl-icon>\n  <hl-icon name=\"UserGroup\" type=\"primary\" size=\"xxl\" fill></hl-icon>\n</hl-group>\n")], -1);

var _hoisted_23 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("h2", {
  id: "tu-biao-ji"
}, [/*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("a", {
  class: "header-anchor",
  href: "#tu-biao-ji"
}), /*#__PURE__*/Object(vue_esm_browser["n" /* createTextVNode */])(" 图标集")], -1);

var _hoisted_24 = /*#__PURE__*/Object(vue_esm_browser["o" /* createVNode */])("p", null, "鸿鸾默认提供了一套常用的 SVG 图标集合，并且拥有更灵活的配置属性。", -1);

var _hoisted_25 = {
  class: "icon-list clearfix"
};
var _hoisted_26 = {
  class: "icon-name"
};

var _hoisted_27 = /*#__PURE__*/Object(vue_esm_browser["m" /* createStaticVNode */])("<h2 id=\"icon-component-options\"><a class=\"header-anchor\" href=\"#icon-component-options\"></a> Icon Component Options</h2><table><thead><tr><th>参数</th><th>说明</th><th>类型</th><th>可选值</th><th>默认值</th></tr></thead><tbody><tr><td>name</td><td>名称</td><td>string</td><td>—</td><td></td></tr><tr><td>color</td><td>颜色</td><td>string</td><td>—</td><td>-</td></tr><tr><td>width</td><td>宽</td><td>string</td><td>—</td><td>-</td></tr><tr><td>height</td><td>高</td><td>string</td><td>—</td><td>-</td></tr><tr><td>size</td><td>大小，width 和 height 优先于 size</td><td>string</td><td>sm/md/lg/xl</td><td>-</td></tr><tr><td>border</td><td>边框粗细</td><td>number</td><td>—</td><td>1.5</td></tr><tr><td>opacity</td><td>填充部分透明度</td><td>number</td><td>—</td><td>0.2</td></tr><tr><td>type</td><td>状态色</td><td>string</td><td>—</td><td>-</td></tr><tr><td>fill</td><td>开启背景填充样式</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>gradient</td><td>开启 Icon 渐变属性</td><td>boolean</td><td>—</td><td>false</td></tr><tr><td>start-color</td><td>渐变按钮的启示颜色</td><td>string</td><td>—</td><td>-</td></tr></tbody></table>", 2);

function iconvue_type_template_id_5810ea12_render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_hl_demo0 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo0");

  var _component_demo_block = Object(vue_esm_browser["P" /* resolveComponent */])("demo-block");

  var _component_hl_demo1 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo1");

  var _component_hl_demo2 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo2");

  var _component_hl_demo3 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo3");

  var _component_hl_demo4 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo4");

  var _component_hl_demo5 = Object(vue_esm_browser["P" /* resolveComponent */])("hl-demo5");

  var _component_hl_icon = Object(vue_esm_browser["P" /* resolveComponent */])("hl-icon");

  var _component_right_nav = Object(vue_esm_browser["P" /* resolveComponent */])("right-nav");

  return Object(vue_esm_browser["G" /* openBlock */])(), Object(vue_esm_browser["j" /* createBlock */])("section", iconvue_type_template_id_5810ea12_hoisted_1, [Object(vue_esm_browser["o" /* createVNode */])("div", iconvue_type_template_id_5810ea12_hoisted_2, [_hoisted_3, _hoisted_4, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo0)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_5];
    }),
    _: 1
  }), _hoisted_6, _hoisted_7, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo1)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_8];
    }),
    _: 1
  }), _hoisted_9, _hoisted_10, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo2)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_11];
    }),
    _: 1
  }), _hoisted_12, _hoisted_13, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, {
    dark: ""
  }, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo3)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_15];
    }),
    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_14];
    }),
    _: 1
  }), _hoisted_16, _hoisted_17, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo4)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_19];
    }),
    default: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_18];
    }),
    _: 1
  }), _hoisted_20, _hoisted_21, Object(vue_esm_browser["o" /* createVNode */])(_component_demo_block, null, {
    source: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_demo5)];
    }),
    highlight: Object(vue_esm_browser["eb" /* withCtx */])(function () {
      return [_hoisted_22];
    }),
    _: 1
  }), _hoisted_23, _hoisted_24, Object(vue_esm_browser["o" /* createVNode */])("ul", _hoisted_25, [(Object(vue_esm_browser["G" /* openBlock */])(true), Object(vue_esm_browser["j" /* createBlock */])(vue_esm_browser["b" /* Fragment */], null, Object(vue_esm_browser["N" /* renderList */])(_ctx.$icon, function (name) {
    return Object(vue_esm_browser["G" /* openBlock */])(), Object(vue_esm_browser["j" /* createBlock */])("li", {
      key: name
    }, [Object(vue_esm_browser["o" /* createVNode */])("span", null, [Object(vue_esm_browser["o" /* createVNode */])("div", null, [Object(vue_esm_browser["o" /* createVNode */])(_component_hl_icon, {
      name: name,
      opacity: .2,
      border: 1.5
    }, null, 8, ["name", "border"])]), Object(vue_esm_browser["o" /* createVNode */])("span", _hoisted_26, Object(vue_esm_browser["T" /* toDisplayString */])(name), 1)])]);
  }), 128))]), _hoisted_27]), Object(vue_esm_browser["o" /* createVNode */])(_component_right_nav)]);
}
// CONCATENATED MODULE: ./website/docs/zh-CN/icon.md?vue&type=template&id=5810ea12

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/extends.js
var helpers_extends = __webpack_require__(4);
var extends_default = /*#__PURE__*/__webpack_require__.n(helpers_extends);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/dist??ref--2-0!./website/md-loader!./website/docs/zh-CN/icon.md?vue&type=script&lang=ts


/* harmony default export */ var iconvue_type_script_lang_ts = ({
  name: 'component-doc',
  components: {
    "hl-demo0": function () {
      var _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      function render(_ctx, _cache) {
        var _component_hl_icon = _resolveComponent("hl-icon");

        return _openBlock(), _createBlock(_component_hl_icon, {
          name: "Home",
          size: "lg"
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo1": function () {
      var _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      function render(_ctx, _cache) {
        var _component_hl_icon = _resolveComponent("hl-icon");

        var _component_hl_group = _resolveComponent("hl-group");

        return _openBlock(), _createBlock(_component_hl_group, {
          indent: "var(--md)"
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_icon, {
              name: "Delete",
              size: "xxs"
            }), _createVNode(_component_hl_icon, {
              name: "Delete",
              size: "xs"
            }), _createVNode(_component_hl_icon, {
              name: "Delete",
              size: "sm"
            }), _createVNode(_component_hl_icon, {
              name: "Delete",
              size: "md"
            }), _createVNode(_component_hl_icon, {
              name: "Delete",
              size: "lg"
            }), _createVNode(_component_hl_icon, {
              name: "Delete",
              size: "xl"
            }), _createVNode(_component_hl_icon, {
              name: "Delete",
              size: "xxl"
            }), _createVNode(_component_hl_icon, {
              name: "Delete",
              width: "56px",
              height: "56px"
            })];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo2": function () {
      var _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      function render(_ctx, _cache) {
        var _component_hl_icon = _resolveComponent("hl-icon");

        var _component_hl_group = _resolveComponent("hl-group");

        return _openBlock(), _createBlock(_component_hl_group, {
          full: "",
          indent: "var(--md)"
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_icon, {
              name: "CameraOff",
              type: "primary",
              size: "lg"
            }), _createVNode(_component_hl_icon, {
              name: "Layout",
              type: "danger",
              size: "lg"
            }), _createVNode(_component_hl_icon, {
              name: "UserGroup",
              type: "warning",
              size: "lg"
            }), _createVNode(_component_hl_icon, {
              name: "Server",
              type: "success",
              size: "lg"
            }), _createVNode(_component_hl_icon, {
              name: "News",
              type: "info",
              size: "lg"
            }), _createVNode(_component_hl_icon, {
              name: "Setting",
              type: "danger",
              size: "lg"
            }), _createVNode(_component_hl_icon, {
              name: "Picture",
              type: "success",
              size: "lg"
            }), _createVNode(_component_hl_icon, {
              name: "Talk",
              size: "lg",
              color: "#888"
            })];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo3": function () {
      var _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      function render(_ctx, _cache) {
        var _component_hl_icon = _resolveComponent("hl-icon");

        var _component_hl_group = _resolveComponent("hl-group");

        return _openBlock(), _createBlock(_component_hl_group, {
          full: "",
          indent: "var(--md)"
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_icon, {
              name: "CameraOff",
              type: "primary",
              size: "lg",
              border: "1.2",
              gradient: ""
            }), _createVNode(_component_hl_icon, {
              name: "Layout",
              type: "danger",
              size: "lg",
              border: "1.2",
              gradient: ""
            }), _createVNode(_component_hl_icon, {
              name: "UserGroup",
              type: "warning",
              size: "lg",
              border: "1.2",
              gradient: ""
            }), _createVNode(_component_hl_icon, {
              name: "Server",
              type: "success",
              size: "lg",
              border: "1.2",
              gradient: ""
            }), _createVNode(_component_hl_icon, {
              name: "News",
              type: "info",
              size: "lg",
              border: "1.2",
              gradient: ""
            }), _createVNode(_component_hl_icon, {
              name: "Setting",
              type: "danger",
              size: "lg",
              border: "1.2",
              gradient: ""
            }), _createVNode(_component_hl_icon, {
              name: "Picture",
              type: "success",
              size: "lg",
              border: "1.2",
              gradient: ""
            }), _createVNode(_component_hl_icon, {
              name: "Talk",
              size: "lg",
              border: "1.2",
              "start-color": "#fff",
              color: "#888",
              gradient: ""
            })];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo4": function () {
      var _createVNode = vue_esm_browser["o" /* createVNode */],
          _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      var _hoisted_1 = /*#__PURE__*/_createVNode("p", null, "填充透明度", -1);

      var _hoisted_2 = /*#__PURE__*/_createVNode("p", null, "线条粗细", -1);

      function render(_ctx, _cache) {
        var _component_hl_icon = _resolveComponent("hl-icon");

        var _component_hl_group = _resolveComponent("hl-group");

        var _component_hl_col = _resolveComponent("hl-col");

        var _component_hl_row = _resolveComponent("hl-row");

        return _openBlock(), _createBlock(_component_hl_row, null, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_col, {
              span: "col"
            }, {
              default: _withCtx(function () {
                return [_hoisted_1, _createVNode(_component_hl_group, {
                  indent: "var(--lg)"
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_icon, {
                      name: "Delete",
                      opacity: 0
                    }), _createVNode(_component_hl_icon, {
                      name: "Delete",
                      opacity: .2
                    }), _createVNode(_component_hl_icon, {
                      name: "Delete",
                      opacity: .5
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            }), _createVNode(_component_hl_col, {
              span: "col"
            }, {
              default: _withCtx(function () {
                return [_hoisted_2, _createVNode(_component_hl_group, {
                  indent: "var(--lg)"
                }, {
                  default: _withCtx(function () {
                    return [_createVNode(_component_hl_icon, {
                      name: "Delete",
                      border: 1
                    }), _createVNode(_component_hl_icon, {
                      name: "Delete",
                      border: 1.5
                    }, null, 8, ["border"]), _createVNode(_component_hl_icon, {
                      name: "Delete",
                      border: 2
                    })];
                  }),
                  _: 1
                })];
              }),
              _: 1
            })];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }(),
    "hl-demo5": function () {
      var _resolveComponent = vue_esm_browser["P" /* resolveComponent */],
          _createVNode = vue_esm_browser["o" /* createVNode */],
          _withCtx = vue_esm_browser["eb" /* withCtx */],
          _openBlock = vue_esm_browser["G" /* openBlock */],
          _createBlock = vue_esm_browser["j" /* createBlock */];

      function render(_ctx, _cache) {
        var _component_hl_icon = _resolveComponent("hl-icon");

        var _component_hl_group = _resolveComponent("hl-group");

        return _openBlock(), _createBlock(_component_hl_group, {
          indent: "var(--lg)",
          style: {
            "align-items": "center"
          }
        }, {
          default: _withCtx(function () {
            return [_createVNode(_component_hl_icon, {
              name: "Airplay",
              type: "primary",
              size: "xxs",
              fill: ""
            }), _createVNode(_component_hl_icon, {
              name: "Picture",
              type: "danger",
              size: "xs",
              fill: ""
            }), _createVNode(_component_hl_icon, {
              name: "Briefcase",
              type: "warning",
              size: "md",
              fill: ""
            }), _createVNode(_component_hl_icon, {
              name: "FolderIn",
              type: "success",
              size: "lg",
              fill: ""
            }), _createVNode(_component_hl_icon, {
              name: "Application",
              type: "info",
              size: "xl",
              fill: ""
            }), _createVNode(_component_hl_icon, {
              name: "UserGroup",
              type: "primary",
              size: "xxl",
              fill: ""
            })];
          }),
          _: 1
        });
      }

      var democomponentExport = {};
      return extends_default()({
        render: render
      }, democomponentExport);
    }()
  }
});
// CONCATENATED MODULE: ./website/docs/zh-CN/icon.md?vue&type=script&lang=ts
 
// CONCATENATED MODULE: ./website/docs/zh-CN/icon.md



iconvue_type_script_lang_ts.render = iconvue_type_template_id_5810ea12_render

/* harmony default export */ var icon = __webpack_exports__["default"] = (iconvue_type_script_lang_ts);

/***/ })

}]);